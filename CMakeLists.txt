cmake_minimum_required(VERSION 3.17)
project(repository-analysis-tool
	VERSION 0.0.0
	DESCRIPTION "Repository Analysis Tool"
	)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

# index
include(CTags)
if(CTAGS_PATH AND CSCOPE_PATH)
	setup_ctags_target("${CMAKE_CURRENT_LIST_DIR}/src/*.?pp")
endif()

include(CTest)

add_subdirectory(src/ratlib)
add_subdirectory(src/cli)

