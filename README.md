## Repository Analysis

(C) 2020 Mario Konrad

This software provides the ability to analyze logs of various SCM
to extract information about the repository.

This tool was inspired by Adam Thornhills book, "Software Design X-Rays".


### Usage
Display the help information with

```
rat --help
```

Example of an actual analysis:

```
rat --config dir/config.json dir/logfile.txt -a overview -a file_changes
```


### Analysis Configuration
The analysis configuration is stored in a json file and specified
on the command line.

The current structure is that only analysers and input files must
be specified on the command line, everything else can be listed
in the configuration file. Information like `filter` and `alias`
may only be specified in the configuration file, there is no
alternative on the command line.


The structure is as follows:
```json
{
	"format": ...
	"filter": ...
	"alias": ...
	"groups": ...
}
```

#### `format`
The input format, same values as for the command line parameter `format`.
Possible values:
- `git`
- `perforce`


#### `filter`
A list of zero or more filters describing white and blacklisting.
Criteria to filter are
- file names
- authors

The filters are regular expressions. White- and blacklist entries are
indicated by leading `+` or `-` signs.

Example of a single whitelist entry for all files:
```json
"filter": [
	{ "+file": ["^.*$"] }
]
```

Example of multiple entries, both white- and blacklist, multiple criteria:
```json
"filter": [
	{ "+file": ["^.*(\\.[ch]|\\.[ch]pp)$"] },
	{ "-author": ["^[A-Z]+$"] }
]
```

Whitelisted are all `.c`, `.h`, `.cpp`, `.hpp` files, but not from users
with username in all capital letters.

The precedence of filters in the list is preserved. In the example above,
the file filter will be applied, then the author filter.


#### `alias`
Mapping of user name aliases, useful if users have more than one username.
This may happen for users with multiple accounts or configured user names.

Example:
```json
"alias": {
	"foo": ["foo1", "foo2"],
	"bar": ["baz", "bif"]
}"
```

The user `foo` is also known as `foo1` and `foo2`. The name resolution
will be the key name, in this case if a commit of `foo1` is found, the user
is identified as `foo`.

The priority is in the following order:
1. Real user names (key names)
2. Aliases

If aliases are not unique, it is unspecified which one is found first.

If the username is not found in the list, it will be taken as is.


#### `groups`
Defining groups, it is possible to reflect the setup of groups of people
or teams within a company. This allows the analysis of changes regarding
teams.

Example:
```json
"groups": [
	{
		"name": "team1",
		"date": "2018-01-01..2018-12-31",
		"members": ["person1", "person2", "person3"]
	},
	{
		"name": "team1",
		"date": "2019-01-01..2019-12-31",
		"members": ["person1", "person2", "person3", "person4"]
	},
	{
		"name": "team2",
		"date": "2018-01-01..2019-12-31",
		"members": ["person5", "person6", "person7"]
	}
]
```


### Reqirements

- C++20 capable compiler (GCC or Clang)


### Dependencies

- boost.program_options
- fmt 6.2.0
- nlohmann/json v3.8.0
- date 2.4.1

