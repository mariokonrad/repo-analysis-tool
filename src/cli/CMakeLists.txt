cmake_minimum_required(VERSION 3.17)
project(rat
	VERSION 0.0.0
	LANGUAGES CXX
	)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

find_package(Boost COMPONENTS program_options REQUIRED)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
	PRIVATE
		cli_arguments.hpp
		cli_arguments.cpp
		main.cpp
	)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_20)
target_compile_options(${PROJECT_NAME}
	PRIVATE
		-Wall
		-Wextra
		-Werror
		-pedantic
	)

target_include_directories(${PROJECT_NAME}
	PRIVATE
		${Boost_INCLUDE_DIRS}
	)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		ratlib::ratlib
		${Boost_LIBRARIES}
		stdc++fs
	)

