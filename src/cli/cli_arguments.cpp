#include "cli_arguments.hpp"
#include <boost/program_options.hpp>

namespace rat
{
// this construct hides boost/program_options.hpp from the world,
// encapsulating it. otherwise it would spread/leak through the
// header file to the user.
struct cli_arguments::program_options {
	boost::program_options::options_description desc_;
	boost::program_options::options_description hidden_;
	boost::program_options::positional_options_description desc_pos_;
	boost::program_options::options_description desc_all_;
	boost::program_options::variables_map vm_;

	program_options()
		: desc_("Options")
		, hidden_("<hidden>")
	{
	}
};

cli_arguments::~cli_arguments() = default;

cli_arguments::cli_arguments()
	: po_(std::make_unique<program_options>())
{
	namespace po = boost::program_options;

	// clang-format off
	po_->desc_.add_options()
		("help,h",
			"shows help information")
		("version,v",
			"shows version")
		("config,c",
			po::value(&opts_.config_file),
			"config file")
		("format,t",
			po::value(&opts_.format_input_file),
			"input format, supported formats: git, perforce")
		("list,l",
			"lists all analysers")
		("analysis,a",
			po::value<std::vector<std::string>>(&opts_.analysers),
			"analysis to execute, following forms are accepted: name, name:param=value:param")
		("help-full",
			"shows help information for the tool and all its features")
		("help-analysis",
			po::value(&opts_.help_analysis),
			"shows help information about specific analysis")
		;
	po_->hidden_.add_options()
		("file",
			po::value<decltype(opts_.files)>(&opts_.files),
			"input file(s)")
		;
	// clang-format on

	po_->desc_pos_.add("file", -1);
	po_->desc_all_.add(po_->desc_).add(po_->hidden_);
}

void cli_arguments::parse(int argc, char ** argv)
{
	namespace po = boost::program_options;

	auto args = po::command_line_parser(argc, argv)
					.options(po_->desc_all_)
					.positional(po_->desc_pos_)
					.run();

	// make sure there were no non-positional `file` options
	for (auto const & opt : args.options) {
		if ((opt.position_key == -1) && (opt.string_key == "file")) {
			throw po::unknown_option("file");
		}
	}

	po::store(args, po_->vm_);
	po::notify(po_->vm_);
}

bool cli_arguments::has_config() const
{
	return po_->vm_.count("config") > 0;
}

bool cli_arguments::has_help_anlaysis() const
{
	return po_->vm_.count("help-analysis") > 0;
}

bool cli_arguments::has_version() const
{
	return po_->vm_.count("version") > 0;
}

bool cli_arguments::has_help() const
{
	return po_->vm_.count("help") > 0;
}

bool cli_arguments::has_list() const
{
	return po_->vm_.count("list") > 0;
}

int cli_arguments::count_input_files() const
{
	return po_->vm_.count("file");
}

int cli_arguments::count_analysers() const
{
	return po_->vm_.count("analysis");
}

bool cli_arguments::has_help_full() const
{
	return po_->vm_.count("help-full");
}

std::ostream & operator<<(std::ostream & os, const cli_arguments & cli)
{
	return os << cli.po_->desc_;
}
}
