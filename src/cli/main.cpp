#include "cli_arguments.hpp"

#include <ratlib/analysis_registry.hpp>
#include <ratlib/config.hpp>
#include <ratlib/filter_utils.hpp>
#include <ratlib/parse.hpp>
#include <ratlib/reporter_stream.hpp>
#include <ratlib/version.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <numeric>
#include <string>
#include <vector>

struct tool_title {};

std::ostream & operator<<(std::ostream & os, const tool_title &)
{
	return os << rat::project_name() << " - Repository Analysis Tool\n\n";
}

struct analysis_help_printer {
	std::string name;
};

std::ostream & operator<<(std::ostream & os, const analysis_help_printer & p)
{
	os << p.name << '\n' << std::string(p.name.size(), '-') << '\n';
	const auto print_help = rat::analysis_registry::help(p.name);
	if (!print_help) {
		os << "not available\n";
	} else {
		print_help(os);
	}
	os << '\n';
	return os;
}

struct help_section_title {
	std::string title;
};

std::ostream & operator<<(std::ostream & os, const help_section_title & section)
{
	return os << section.title << "\n" << std::string(section.title.size(), '=') << '\n';
}

int main(int argc, char ** argv)
{
	rat::config configuration;

	rat::cli_arguments cli;
	try {
		cli.parse(argc, argv);
	} catch (std::exception & e) {
		std::cerr << "error: " << e.what() << '\n';
		return 1;
	}

	if (cli.has_version()) {
		std::cout << rat::project_version() << '\n';
		return 0;
	}

	if (cli.has_help()) {
		std::cout << tool_title();
		std::cout << "usage: rat [Options] file...\n\n" << cli << '\n';
		return 0;
	} else if (cli.has_help_anlaysis()) {
		std::cout << tool_title() << analysis_help_printer{cli.help_analysis()};
		return 0;
	} else if (cli.has_help_full()) {
		std::cout << tool_title();
		std::cout << help_section_title{"Usage"} << '\n';
		std::cout << "rat [Options] file...\n\n" << cli << "\n\n";
		std::cout << help_section_title{"Analysers"} << '\n';
		for (const auto & name : rat::analysis_registry::names())
			std::cout << analysis_help_printer{name} << '\n';
		return 0;
	}

	if (cli.has_list()) {
		std::cout << "available analysers:\n";
		for (const auto & name : rat::analysis_registry::names())
			std::cout << "  " << name << '\n';
		return 0;
	}

	std::string format_input_file = cli.format();
	if (cli.has_config()) {
		configuration = rat::config::from_file(cli.config_file());
		if (format_input_file.empty())
			format_input_file = configuration.format();
	}

	if (format_input_file.empty()) {
		std::cerr << "error: not input format specified\n";
		return 1;
	}

	if (cli.count_input_files() < 1) {
		std::cerr << "error: no input file(s) specificed\n";
		return 1;
	}

	for (const auto & name : cli.analysers()) {
		if (!rat::analysis_registry::check(name)) {
			std::cerr << "error: unsupported analysis: " << name << '\n';
			return 1;
		}
	}

	std::function<std::string(const std::string &)> resolver
		= [](const std::string & author) { return author; };
	if (configuration.authors().size() > 0u)
		resolver = [&configuration](const std::string & alias) -> std::string {
			return configuration.author_for(alias).name;
		};

	std::vector<rat::commit> commits;
	for (const auto & input_file : cli.files()) {
		const auto c = rat::parse(
			format_input_file, input_file, rat::create_filter(configuration), resolver);
		commits.reserve(commits.size() + c.size());
		std::copy(begin(c), end(c), std::back_inserter(commits));
	}

	for (const auto & name : cli.analysers()) {
		rat::reporter_stream rep(std::cout);
		rat::analysis_registry::create(name)->process(rep, commits);
		std::cout << '\n';
	}

	return 0;
}
