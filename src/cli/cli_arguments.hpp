#ifndef RAT_CLI_ARGUMENTS_HPP
#define RAT_CLI_ARGUMENTS_HPP

#include <memory>
#include <string>
#include <vector>

namespace rat
{
class cli_arguments final
{
public:
	~cli_arguments();
	cli_arguments();

	cli_arguments(const cli_arguments &) = delete;
	cli_arguments & operator=(const cli_arguments &) = delete;

	cli_arguments(cli_arguments &&) = default;
	cli_arguments & operator=(cli_arguments &&) = default;

	void parse(int argc, char ** argv);

	int count_input_files() const;
	int count_analysers() const;

	bool has_version() const;
	bool has_list() const;
	bool has_config() const;
	bool has_help() const;
	bool has_help_anlaysis() const;
	bool has_help_full() const;

	std::string config_file() const { return opts_.config_file; }
	std::string format() const { return opts_.format_input_file; }
	std::string help_analysis() const { return opts_.help_analysis; }
	std::vector<std::string> files() const { return opts_.files; }
	std::vector<std::string> analysers() const { return opts_.analysers; }

	friend std::ostream & operator<<(std::ostream & os, const cli_arguments & cli);

private:
	struct program_options;
	std::unique_ptr<program_options> po_;

	struct {
		std::string config_file;
		std::string format_input_file;
		std::string help_analysis;
		std::vector<std::string> files;
		std::vector<std::string> analysers;
	} opts_;
};
}

#endif
