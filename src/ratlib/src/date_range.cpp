#include <ratlib/date_range.hpp>
#include "sto.hpp"
#include <regex>
#include <stdexcept>

namespace rat
{
namespace
{
template <typename T> static T value(const std::string & s, T default_value)
{
	return s.empty() ? default_value : T(sto<uint64_t>(s));
}

date::year_month_day parse_floor(
	const std::string & ys, const std::string & ms, const std::string & ds)
{
	const auto y = value(ys, date::year(0));
	const auto m = value(ms, date::January);
	const auto d = value(ds, date::day(1));

	date::year_month_day ymd{y, m, d};
	if (!ymd.ok())
		throw std::invalid_argument{"invalid date"};

	return ymd;
}

date::year_month_day parse_ceil(
	const std::string & ys, const std::string & ms, const std::string & ds)
{
	const auto y = value(ys, date::year(9999));
	const auto m = value(ms, date::December);
	const auto d = value(ds, (date::year_month(y, m) / date::last).day());

	date::year_month_day ymd{y, m, d};
	if (!ymd.ok())
		throw std::invalid_argument{"invalid date"};

	return ymd;
}
}

date_range::date_range(const date::year_month_day & f, const date::year_month_day & t)
	: from_(f)
	, to_(t)
{
	if (from_ > to_)
		throw std::invalid_argument {"'from' greater 'to'"};
}

const date::year_month_day & date_range::from() const noexcept
{
	return from_;
}

const date::year_month_day & date_range::to() const noexcept
{
	return to_;
}

bool date_range::contains(const date::year_month_day & d) const
{
	return (d >= from()) && (d <= to());
}

bool date_range::overlaps(const date_range & other) const
{
	return contains(other.from()) | contains(other.to()) | other.contains(from())
		| other.contains(to());
}

date_range date_range::parse(const std::string & s)
{
	// clang-format off
	static const std::basic_regex<char> pattern(
		// date_range
		"(([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?)?"
		"\\.\\."
		"(([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?)?"
		// single_date
		"|([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?"
		);
	// clang-format on

	std::cmatch result;
	const auto rc = regex_match(s.c_str(), result, pattern);
	if (!rc)
		throw std::runtime_error {"parse error (regex)"};

	if (result.size() != 18u)
		throw std::runtime_error {"parse error (number of fields)"};

	// single date
	if (result[13].length())
		return {parse_floor(result[13].str(), result[15].str(), result[17].str()),
			parse_ceil(result[13].str(), result[15].str(), result[17].str())};

	// date_range
	return {parse_floor(result[2].str(), result[4].str(), result[6].str()),
		parse_ceil(result[8].str(), result[10].str(), result[12].str())};
}
}
