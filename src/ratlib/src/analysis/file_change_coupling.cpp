#include "file_change_coupling.hpp"
#include "analysis_parameters.hpp"
#include "algorithm_unique_emplace_back.hpp"
#include <ratlib/reporter.hpp>
#include <ratlib/percent.hpp>
#include <fmt/format.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#include <vector>

namespace rat
{
namespace
{
static std::unique_ptr<analysis> create(const analysis_parameters & params)
{
	auto threshold = params.get("threshold", file_change_coupling::default_threshold);
	auto commits = params.get("commits", file_change_coupling::default_commits);
	auto percent = params.get("percent", file_change_coupling::default_percent);
	return std::make_unique<file_change_coupling>(threshold, commits, percent);
}

static void help(std::ostream & os)
{
	os << '\n';
	os << "Parameters:\n";
	os << "  threshold\n";
	os << "      shows files only if number of changes was at least this value.\n";
	os << "      must be greater than zero.\n";
	os << "      default: " << file_change_coupling::default_threshold << '\n';
	os << '\n';
	os << "  commits\n";
	os << "      change window size. number of commits where files must change\n";
	os << "      in order to considered as potential coupling.\n";
	os << "      must be greater than zero.\n";
	os << "      default: " << file_change_coupling::default_commits << '\n';
	os << '\n';
	os << "  percent\n";
	os << "      percent of cochanges. Files are potentially considered coupled if they\n";
	os << "      change at least this percentige togther of changes to either file.\n";
	os << "      must be in the range 1..100 (inclusive).\n";
	os << "      default: " << file_change_coupling::default_percent << '\n';
	os << '\n';
}
}

analysis_descriptor file_change_coupling::descriptor()
{
	return {[] { return "file_change_coupling"; }, create, help};
}

file_change_coupling::file_change_coupling(
	std::uint64_t threshold, std::uint64_t commits, std::uint64_t percent)
	: threshold_(threshold)
	, commits_(commits)
	, percent_(percent)
{
	if (threshold_ < 1u)
		throw std::runtime_error {"invalid value for 'threshold'"};
	if (commits_ < 1u)
		throw std::runtime_error {"invalid value for 'commits'"};
	if (percent_ < 1u || percent_ > 100u)
		throw std::runtime_error {"invalid value for 'percent'"};
}

bool operator==(const change_count & a, const change_count & b)
{
	return a.name == b.name;
}

namespace detail
{
std::vector<std::string> file_list_in_span(std::vector<commit>::const_iterator first,
	std::vector<commit>::const_iterator last, std::uint64_t threshold)
{
	std::map<std::string, std::uint64_t> changes;
	for (auto i = first; i != last; ++i) {
		for (const auto & f : i->files) {
			changes[f.filename]++;
		}
	}

	std::vector<std::string> files;
	for (const auto & [f, n] : changes)
		if (n >= threshold)
			files.emplace_back(f);

	return files;
}

static bool is_file_changed_in_commit(const commit & c, const std::string & filename)
{
	auto cmp = [&](const auto & change) { return filename == change.filename; };
	return std::find_if(begin(c.files), end(c.files), cmp) != end(c.files);
}

change_matrix span_change_matrix(std::vector<commit>::const_iterator first,
	std::vector<commit>::const_iterator last, const std::vector<std::string> & files)
{
	change_matrix m(files.size());
	for (auto c = first; c != last; ++c) {
		for (auto i = 0u; i < files.size(); ++i) {
			if (is_file_changed_in_commit(*c, files[i])) {
				for (auto j = 0u; j < files.size(); ++j) {
					if (is_file_changed_in_commit(*c, files[j]))
						++m(i, j);
				}
			}
		}
	}
	return m;
}

change_matrix::change_matrix(const std::vector<std::vector<value_type>> & other)
	: n_(other.size())
	, data_(n_ * n_, 0u)
{
	for (auto i = 0u; i < n_; ++i) {
		if (other[i].size() != n_)
			throw std::runtime_error {"not a quadratic matrix"};
		for (auto j = 0u; j < n_; ++j)
			operator()(i, j) = other[i][j];
	}
}

void change_matrix::dump(std::ostream & os) const
{
	for (auto i = 0u; i < n_; ++i) {
		for (auto j = 0u; j < n_; ++j)
			os << std::setw(4) << operator()(i, j);
		os << '\n';
	}
}

static void report_coupling(reporter & rep, const coupling & c)
{
	rep.push_back_group_start();
	for (const auto & f : c.files)
		rep.push_back({percent{c.percent}, f.count, f.name});
	rep.push_back_group_end();
}

struct change_count_cmp {
	bool operator()(const change_count & a, const change_count & b) const noexcept
	{
		return a.name < b.name;
	}
};

std::vector<coupling> reduce(const std::vector<couplings_commit_span> & spans)
{
	std::vector<coupling> v;
	for (const auto & s : spans) {
		for (const auto & c : s.couplings) {

			bool must_insert = true;
			for (auto & a : v) {

				// c could be a subset of a. if it is the same set, update percent if necessary
				if (std::includes(begin(a.files), end(a.files), begin(c.files), end(c.files),
						change_count_cmp())) {
					if ((c.files.size() == a.files.size()) && (c.percent > a.percent)) {
						a.percent = c.percent;
					}
					must_insert = false;
					break;
				}

				// a could be a subset of c, c may be larger therefore must replace a
				if (std::includes(begin(c.files), end(c.files), begin(a.files), end(a.files),
						change_count_cmp())) {
					a = c;
					must_insert = false;
					break;
				}
			}

			// if no "similar" subset was found, it's obviously a new one
			if (must_insert)
				v.push_back(c);
		}
	}

	return v;
}

bool operator==(const coupling & a, const coupling & b)
{
	return a.files == b.files;
}

bool operator!=(const coupling & a, const coupling & b)
{
	return !(a == b);
}
}

void file_change_coupling::process(reporter & rep, const std::vector<commit> & commits)
{
	using namespace detail;

	if (commits.size() < commits_)
		return;

	std::vector<couplings_commit_span> spans;

	auto span_first = begin(commits);
	auto span_last = std::next(span_first, commits_);
	for (; span_last <= end(commits); ++span_first, ++span_last) {

		const auto files = detail::file_list_in_span(span_first, span_last, threshold_);
		const auto m = detail::span_change_matrix(span_first, span_last, files);

		if (m.n() <= 1u) // only one file, coupling information makes no sense
			continue;

		couplings_commit_span span_couplings{*span_first, *std::prev(span_last), {}};

		for (detail::change_matrix::size_type i = 0u; i < m.n() - 1u; ++i) {
			coupling entry;

			for (detail::change_matrix::size_type j = i + 1u; j < m.n(); ++j) {
				const auto pi = 100 * m(i, j) / m(i, i);
				const auto pj = 100 * m(i, j) / m(j, j);
				const auto p = std::min(pi, pj); // minimum value for coupling

				if (p < percent_)
					continue;

				unique_emplace_back(
					entry.files,
					[filename = files[i]](const auto & f) { return f.name == filename; },
					files[i], m(i, i));

				unique_emplace_back(
					entry.files,
					[filename = files[j]](const auto & f) { return f.name == filename; },
					files[j], m(j, j));

				entry.percent = std::max(entry.percent, p);
			}

			if (!entry.files.empty()) {
				std::sort(begin(entry.files), end(entry.files), change_count_cmp());
				span_couplings.couplings.push_back(entry);
			}
		}

		std::sort(begin(span_couplings.couplings), end(span_couplings.couplings),
			[](const auto & a, const auto & b) { return a.percent > b.percent; });

		if (!span_couplings.couplings.empty())
			spans.push_back(span_couplings);
	}

	rep.set_fields({
		"percent", // rat::percent
		"count", // std::uint64_t
		"name" // std::string
	});
	for (const auto & c : reduce(spans))
		report_coupling(rep, c);
}
}
