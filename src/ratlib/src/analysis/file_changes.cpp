#include "file_changes.hpp"
#include "analysis_parameters.hpp"
#include "change_count.hpp"
#include <ratlib/reporter.hpp>
#include <fmt/format.h>
#include <iostream>
#include <map>

namespace rat
{
namespace
{
static std::unique_ptr<analysis> create(const analysis_parameters & params)
{
	auto threshold = params.get("threshold", file_changes::default_threshold);
	return std::make_unique<file_changes>(threshold);
}

static void help(std::ostream & os)
{
	os << '\n';
	os << "Parameters:\n";
	os << "  threshold\n";
	os << "      shows files only if number of changes was at least this value.\n";
	os << "      must be at least zero.\n";
	os << "      default: " << file_changes::default_threshold << '\n';
	os << '\n';
}
}

analysis_descriptor file_changes::descriptor()
{
	return {[] { return "file_changes"; }, create, help};
}

static std::map<std::string, std::uint64_t> reduce_file_changes(
	const std::vector<commit> & commits)
{
	std::map<std::string, std::uint64_t> changes;
	for (const auto & c : commits) {
		for (const auto & f : c.files) {
			changes[f.filename]++;
		}
	}
	return changes;
}

file_changes::file_changes(std::uint64_t threshold)
	: threshold_(threshold)
{
}

void file_changes::process(reporter & rep, const std::vector<commit> & commits)
{
	const auto changes = reduce_file_changes(commits);

	std::vector<change_count> files;
	for (const auto & [f, n] : changes)
		if (n >= threshold_)
			files.emplace_back(f, n);

	std::sort(begin(files), end(files),
		[](const auto & c1, const auto & c2) { return c1.count > c2.count; });

	rep.insert("threshold", threshold_);
	rep.insert("number of changed files over threshold", files.size());

	rep.set_fields({
		"count", // std::uint64_t
		"name" // std::string
	});

	for (const auto & file_change : files)
		rep.push_back({file_change.count, file_change.name});
}
}

