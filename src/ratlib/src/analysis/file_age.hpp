#ifndef RAG_ANALYSIS_FILE_AGE_HPP
#define RAG_ANALYSIS_FILE_AGE_HPP

#include <ratlib/analysis.hpp>

namespace rat
{
class file_age : public analysis
{
public:
	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();
};
}

#endif
