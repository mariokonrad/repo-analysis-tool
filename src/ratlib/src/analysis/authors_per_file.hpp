#ifndef RAG_ANALYSIS_AUTHORS_PER_FILE_HPP
#define RAG_ANALYSIS_AUTHORS_PER_FILE_HPP

#include <ratlib/analysis.hpp>

namespace rat
{
class authors_per_changed_file : public analysis
{
public:
	static constexpr const std::uint64_t default_threshold = 4u;

	authors_per_changed_file() = default;
	authors_per_changed_file(std::uint64_t threshold);

	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();

private:
	std::uint64_t threshold_ = default_threshold;
};
}

#endif
