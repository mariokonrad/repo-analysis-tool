#ifndef RAT_ANALYSIS_OVERVIEW_HPP
#define RAT_ANALYSIS_OVERVIEW_HPP

#include <ratlib/analysis.hpp>

namespace rat
{
class overview : public analysis
{
public:
	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();
};
}

#endif
