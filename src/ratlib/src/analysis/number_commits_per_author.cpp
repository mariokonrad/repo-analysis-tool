#include "number_commits_per_author.hpp"
#include "change_count.hpp"
#include <ratlib/reporter.hpp>
#include <fmt/format.h>
#include <algorithm>
#include <map>

namespace rat
{
analysis_descriptor number_commits_per_author::descriptor()
{
	return {[] { return "number_commits_per_author"; },
		[](const analysis_parameters &) {
			return std::make_unique<number_commits_per_author>();
		},
		{}};
}

void number_commits_per_author::process(reporter & rep, const std::vector<commit> & commits)
{
	std::map<std::string, std::uint64_t> t;
	for (const auto & c : commits)
		t[c.head.committer]++;

	std::vector<change_count> result;
	result.reserve(t.size());
	for (const auto & [name, count] : t)
		result.emplace_back(name, count);

	std::sort(begin(result), end(result),
		[](const auto & c1, const auto & c2) { return c1.count > c2.count; });

	rep.set_fields({
		"count", // std::uint64_t
		"name" // std::string
	});

	for (const auto & [name, count] : result)
		rep.push_back({count, name});
}
}
