#include "file_age.hpp"
#include <ratlib/reporter.hpp>
#include <date/date.h>
#include <fmt/format.h>
#include <chrono>
#include <memory>
#include <vector>

template <> struct fmt::formatter<date::year_month_day> {
	constexpr auto parse(format_parse_context & ctx) { return ctx.begin(); }

	template <typename format_context>
	auto format(const date::year_month_day & d, format_context & ctx)
	{
		return format_to(ctx.out(), "{:04}-{:02}-{:02}", static_cast<int>(d.year()),
			static_cast<unsigned>(d.month()), static_cast<unsigned>(d.day()));
	}
};

namespace rat
{
analysis_descriptor file_age::descriptor()
{
	return {[] { return "file_age"; },
		[](const analysis_parameters &) -> std::unique_ptr<analysis> {
			return std::make_unique<file_age>();
		},
		{}};
}

namespace
{
struct file_info {
	file_info(
		const std::string & fn, const date::year_month_day & n, const date::year_month_day & o)
		: filename(fn)
		, newest(n)
		, oldest(o)
	{
	}

	std::string filename;
	date::year_month_day newest;
	date::year_month_day oldest;
	std::uint64_t num_changes = 1u;

	// redundant information but helpful to prevent an additional pass through the data.
	std::uint64_t diff = 0u;
};
}

void file_age::process(reporter & rep, const std::vector<commit> & commits)
{
	std::vector<file_info> data;
	auto latest_change = date::year(1970) / date::January / 1;

	for (const auto & c : commits) {
		// for each file, min/max dates
		for (const auto & f : c.files) {
			auto i = find_if(begin(data), end(data),
				[&f](const file_info & info) { return f.filename == info.filename; });

			latest_change = std::max(latest_change, c.head.date);

			if (i == end(data)) {
				data.emplace_back(f.filename, c.head.date, c.head.date);
			} else {
				i->newest = std::max(i->newest, c.head.date);
				i->oldest = std::min(i->oldest, c.head.date);
				i->diff = (date::sys_days(i->newest) - date::sys_days(i->oldest)).count();
				i->num_changes++;
			}
		}
	}

	sort(begin(data), end(data), [](const auto & a, const auto & b) {
		return std::tie(a.diff, a.filename) > std::tie(b.diff, b.filename);
	});

	rep.set_fields({
		"diff days", // std::uint64_t
		"days since last change", // std::uint64_t
		"num changes", // std::uint64_t
		"oldest", // std::string
		"newest", // std::string
		"name" // std::string
	});

	for (const auto & e : data) {
		const std::uint64_t dchange = (date::sys_days(latest_change) - date::sys_days(e.newest)).count();

		rep.push_back({e.diff, dchange, e.num_changes, fmt::format("{}", e.oldest),
			fmt::format("{}", e.newest), e.filename});
	}
}
}

