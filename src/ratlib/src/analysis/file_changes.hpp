#ifndef RAT_ANALYSIS_FILE_CHANGES_HPP
#define RAT_ANALYSIS_FILE_CHANGES_HPP

#include <ratlib/analysis.hpp>

namespace rat
{
class file_changes : public analysis
{
public:
	static constexpr const std::uint64_t default_threshold = 4u;

	file_changes() = default;
	file_changes(std::uint64_t threshold);

	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();

private:
	std::uint64_t threshold_ = default_threshold;
};
}

#endif
