#ifndef RAT_ANALYSIS_NUMBER_COMMITS_PER_AUTHOR_HPP
#define RAT_ANALYSIS_NUMBER_COMMITS_PER_AUTHOR_HPP

#include <ratlib/analysis.hpp>

namespace rat
{
class number_commits_per_author : public analysis
{
public:
	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();
};
}

#endif
