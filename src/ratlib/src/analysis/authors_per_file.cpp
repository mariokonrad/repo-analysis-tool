#include "authors_per_file.hpp"
#include "analysis_parameters.hpp"
#include <ratlib/reporter.hpp>
#include <fmt/format.h>
#include <algorithm>
#include <iostream>
#include <memory>
#include <map>
#include <set>

namespace rat
{
namespace
{
static std::unique_ptr<analysis> create(const analysis_parameters & params)
{
	auto threshold = params.get("threshold", authors_per_changed_file::default_threshold);
	return std::make_unique<authors_per_changed_file>(threshold);
}

static void help(std::ostream & os)
{
	os << '\n';
	os << "Parameters:\n";
	os << "  threshold\n";
	os << "      shows files only if number of authors was at least this value.\n";
	os << "      must be at least zero.\n";
	os << "      default: " << authors_per_changed_file::default_threshold << '\n';
	os << '\n';
}
}

analysis_descriptor authors_per_changed_file::descriptor()
{
	return {[] { return "authors_per_file"; }, create, help};
}

struct entry {
	std::uint64_t num_authors;
	std::string file;

	entry(std::uint64_t n, const std::string & filename)
		: num_authors(n)
		, file(filename)
	{
	}

	bool operator<(const entry & other) const
	{
		return std::tie(num_authors, file) < std::tie(other.num_authors, other.file);
	}

	bool operator>(const entry & other) const
	{
		return std::tie(num_authors, file) > std::tie(other.num_authors, other.file);
	}
};

authors_per_changed_file::authors_per_changed_file(std::uint64_t threshold)
	: threshold_(threshold)
{
}

void authors_per_changed_file::process(reporter & rep, const std::vector<commit> & commits)
{
	std::map<std::string, std::set<std::string>> file_authors;
	for (const auto & c : commits) {
		for (const auto & f : c.files) {
			file_authors[f.filename].insert(c.head.committer);
		}
	}

	std::vector<entry> entries;
	for (const auto & fa : file_authors) {
		if (fa.second.size() < threshold_)
			continue;
		entries.emplace_back(fa.second.size(), fa.first);
	}
	std::sort(begin(entries), end(entries), std::greater<>());

	rep.set_fields({
		"num authors", // std::uint64_t
		"authors", // std::string
		"filename" // std::string
	});

	for (const auto & e : entries) {
		rep.push_back({
			e.num_authors,
			fmt::format("{}", fmt::join(file_authors[e.file], ";")),
			e.file});
	}
}
}
