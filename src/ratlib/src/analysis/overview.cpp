#include "overview.hpp"
#include "statistics.hpp"
#include <ratlib/reporter.hpp>
#include <memory>
#include <set>

namespace rat
{
analysis_descriptor overview::descriptor()
{
	return {[] { return "overview"; },
		[](const analysis_parameters &) -> std::unique_ptr<analysis> {
			return std::make_unique<overview>();
		},
		{}};
}

void overview::process(reporter & rep, const std::vector<commit> & commits)
{
	std::uint64_t total_file_changes = 0u;
	std::vector<std::uint64_t> num_changed_files_per_commit;
	std::set<std::string> files;
	std::set<std::string> authors;

	num_changed_files_per_commit.reserve(commits.size());

	for (const auto & c : commits) {
		authors.insert(c.head.committer);
		total_file_changes += c.files.size();
		num_changed_files_per_commit.push_back(c.files.size());
		for (const auto & f : c.files) {
			files.insert(f.filename);
		}
	}

	std::sort(begin(num_changed_files_per_commit), end(num_changed_files_per_commit));

	const auto m
	    = median(begin(num_changed_files_per_commit), end(num_changed_files_per_commit));
	const auto [avg, stddev]
	    = average(begin(num_changed_files_per_commit), end(num_changed_files_per_commit));

	rep.insert("commits", commits.size());
	rep.insert("authors", authors.size());
	rep.insert("changed files unique", files.size());
	rep.insert("changed files total", total_file_changes);
	rep.insert("median number changed files per commit", m);
	rep.insert("average number change files per commit", avg);
	rep.insert("stddev number change files per commit", stddev);
}
}
