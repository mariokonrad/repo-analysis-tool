#ifndef RAT_ANALYSIS_FILE_CHANGE_COUPLING_HPP
#define RAT_ANALYSIS_FILE_CHANGE_COUPLING_HPP

#include <ratlib/analysis.hpp>
#include "change_count.hpp"
#include <vector>

namespace rat
{
class file_change_coupling : public analysis
{
public:
	static constexpr const std::uint64_t default_threshold = 4u;
	static constexpr const std::uint64_t default_commits = 20u;
	static constexpr const std::uint64_t default_percent = 50u;

	file_change_coupling() = default;
	file_change_coupling(
		std::uint64_t threshold, std::uint64_t commits, std::uint64_t percent_cochange);

	virtual void process(reporter & rep, const std::vector<commit> & commits) override;

	static analysis_descriptor descriptor();

private:
	std::uint64_t threshold_ = default_threshold;
	std::uint64_t commits_ = default_commits;
	std::uint64_t percent_ = default_percent;
};

// details also in header to be used in tests
namespace detail
{
class change_matrix
{
private:
	using container_type = std::vector<std::vector<std::string>::size_type>;

public:
	using value_type = container_type::value_type;
	using size_type = container_type::size_type;

	explicit change_matrix(size_type n) noexcept
		: n_(n)
		, data_(n * n, 0u)
	{
	}

	change_matrix(const std::vector<std::vector<value_type>> & other);

	size_type n() const { return n_; }

	value_type & operator()(size_type i, size_type j) noexcept
	{
		return data_[n_ * i + j];
	}

	const value_type & operator()(size_type i, size_type j) const noexcept
	{
		return data_[n_ * i + j];
	}

	bool operator==(const change_matrix & other) const noexcept { return data_ == other.data_; }
	bool operator!=(const change_matrix & other) const noexcept { return !(*this == other); }

	void dump(std::ostream &) const;

private:
	const size_type n_;
	std::vector<std::vector<std::string>::size_type> data_;
};

change_matrix span_change_matrix(std::vector<commit>::const_iterator first,
	std::vector<commit>::const_iterator last, const std::vector<std::string> & files);

std::vector<std::string> file_list_in_span(std::vector<commit>::const_iterator first,
	std::vector<commit>::const_iterator last, std::uint64_t threshold);

struct coupling {
	std::vector<change_count> files;
	std::uint64_t percent = 0u;

	friend bool operator==(const coupling & a, const coupling & b);
	friend bool operator!=(const coupling & a, const coupling & b);
};

struct couplings_commit_span {
	commit first;
	commit last;
	std::vector<coupling> couplings;
};

std::vector<coupling> reduce(const std::vector<couplings_commit_span> & spans);
}
}

#endif
