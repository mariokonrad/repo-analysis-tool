#include "filter.hpp"
#include <ratlib/commit.hpp>
#include <stdexcept>

namespace rat
{
namespace
{
static std::tuple<filter_type, std::string> split_filter_id(const std::string & id)
{
	if (id.size() < 2u)
		throw std::invalid_argument{"invalid filter id format: " + std::string {id}};
	auto c = id[0];
	if (c == '-')
		return {filter_type::blacklist, id.substr(1u)};
	if (c == '+')
		return {filter_type::whitelist, id.substr(1u)};
	throw std::runtime_error {"invalid filter type: " + std::string {c}};
}
}

std::unique_ptr<filter> make_filter(const std::string & id, const std::string & rule)
{
	const auto [type, name] = split_filter_id(id);
	if (name == "file")
		return std::make_unique<file_filter>(type, rule);
	if (name == "author")
		return std::make_unique<author_filter>(type, rule);
	throw std::invalid_argument {"unknown filter: " + id};
}

filter::~filter() {}

filter::filter(filter_type type, const std::string & rule)
	: type_(type)
	, regex_(rule, std::regex_constants::ECMAScript | std::regex_constants::icase)
{
}

filter_type filter::type() const
{
	return type_;
}

bool filter::check(const commit_head & head, const std::string & filepath) const
{
	switch (type()) {
		case filter_type::whitelist:
			return matches(regex_, head, filepath);
		case filter_type::blacklist:
			return !matches(regex_, head, filepath);
	}
	throw std::logic_error {"unreachable"};
}

bool filter::operator()(const commit_head & head, const std::string & filepath) const
{
	return check(head, filepath);
}

bool file_filter::matches(
	const std::regex & pattern, const commit_head &, const std::string & filepath) const
{
	return std::regex_match(filepath, pattern);
}

bool author_filter::matches(
	const std::regex & pattern, const commit_head & head, const std::string &) const
{
	return std::regex_match(head.committer, pattern);
}
}
