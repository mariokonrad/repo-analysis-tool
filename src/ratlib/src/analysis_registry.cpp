#include <ratlib/analysis_registry.hpp>
#include "analysis_parameters.hpp"
#include "string_split.hpp"
#include "analysis/authors_per_file.hpp"
#include "analysis/file_age.hpp"
#include "analysis/file_changes.hpp"
#include "analysis/file_change_coupling.hpp"
#include "analysis/number_commits_per_author.hpp"
#include "analysis/overview.hpp"
#include <map>

namespace rat
{
namespace
{
static std::tuple<std::string, std::vector<std::string>> split_analysis_description(
	const std::string & s)
{
	auto parts = utils::split<std::vector<std::string>>(s, ":");
	if (parts.size() < 1u)
		throw std::runtime_error {"invalid analysis description: " + s};

	const auto name = parts[0];
	parts.erase(parts.begin());

	return {name, parts};
}

const std::map<std::string, analysis_descriptor> & map()
{
	// scott meyers singleton
	static const std::map<std::string, analysis_descriptor> map_ = {
		{authors_per_changed_file::descriptor().name(), authors_per_changed_file::descriptor()},
		{file_age::descriptor().name(), file_age::descriptor()},
		{file_change_coupling::descriptor().name(), file_change_coupling::descriptor()},
		{file_changes::descriptor().name(), file_changes::descriptor()},
		{number_commits_per_author::descriptor().name(),
			number_commits_per_author::descriptor()},
		{overview::descriptor().name(), overview::descriptor()}};
	return map_;
}
}

bool analysis_registry::check(const std::string & name_and_config)
{
	const auto & [name, params] = split_analysis_description(name_and_config);
	return map().find(name) != map().end();
}

std::unique_ptr<analysis> analysis_registry::create(const std::string & name_and_config)
{
	const auto & [name, params] = split_analysis_description(name_and_config);
	const auto parameters = analysis_parameters(params);

	const auto i = map().find(name);
	if (i == map().end())
		return {};
	return i->second.create(parameters);
}

std::vector<std::string> analysis_registry::names()
{
	std::vector<std::string> names;
	names.reserve(map().size());
	for (const auto & item : map())
		names.emplace_back(item.first);
	return names;
}

std::function<void(std::ostream &)> analysis_registry::help(const std::string & name)
{
	const auto i = map().find(name);
	if (i == map().end())
		throw std::runtime_error {"invalid analysis name: " + name};
	return i->second.help;
}
}
