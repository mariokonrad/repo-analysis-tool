#include <ratlib/percent.hpp>
#include <fmt/format.h>

namespace rat
{
std::string to_string(const percent & p)
{
	return fmt::format("{:3}%", p.value());
}
}

