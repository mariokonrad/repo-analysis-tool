#ifndef RAT_ALGORITHM_COPY_JOIN_HPP
#define RAT_ALGORITHM_COPY_JOIN_HPP

#include <iterator>

namespace rat
{
// to be replaced with std::ostream_joiner once it gets out of experimental

template <class Iterator, class OutputIterator,
	class ValueType = typename std::iterator_traits<OutputIterator>::value_type>
void copy_join(Iterator first, Iterator last, OutputIterator out, ValueType && delm)
{
	for (; first != last; ++first, ++out) {
		*out = *first;
		if (std::next(first) != last)
			*out = delm;
	}
}
}

#endif
