#include "analysis_parameters.hpp"

namespace rat
{
analysis_parameters::analysis_parameters(const std::vector<std::string> & params)
{
	params_.reserve(params.size());
	for (const auto & p : params)
		params_.emplace_back(split(p));
}

analysis_parameters::key_value analysis_parameters::split(const std::string & param)
{
	const auto p = param.find_first_of("=");
	auto key = param.substr(0u, p);
	if (p >= param.size())
		return {key, {}};
	return {key, param.substr(p + 1)};
}
}

