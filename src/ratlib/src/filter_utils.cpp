#include <ratlib/filter_utils.hpp>
#include <ratlib/config.hpp>
#include "filter.hpp"
#include <memory>
#include <vector>

namespace rat
{
auto create_filter(const config & cfg)
	-> std::function<bool(const commit_head &, const std::string &)>
{
	std::vector<std::shared_ptr<filter>> filters;
	for (const auto & fd : cfg.filters()) {
		for (const auto & f : fd.rules) {
			filters.push_back(make_filter(fd.id, f));
		}
	}

	return [filters = std::move(filters)](
			   const commit_head & head, const std::string & filepath) -> bool {
		return check_filter_chain(begin(filters), end(filters), head, filepath);
	};
}
}

