#include <ratlib/reporter_stream.hpp>
#include <fmt/format.h>
#include <algorithm>
#include <iostream>
#include <stdexcept>

namespace rat
{
namespace
{
template <class... Ts> struct overloaded : Ts... {
	using Ts::operator()...;
};
template <class... Ts> overloaded(Ts...)->overloaded<Ts...>;

static std::string to_string(const reporter::entry & e)
{
	// clang-format off
	return std::visit(overloaded {
		[](std::uint64_t v)       { return std::to_string(v); },
		[](std::int64_t v)        { return std::to_string(v); },
		[](double v)              { return std::to_string(v); },
		[](const std::string & v) { return v; },
		[](percent v)             { return to_string(v); },
		[](...)                   { throw std::logic_error {"unhandled type in visit"}; }
		}, e);
	// clang-format on
}

static std::string format(const reporter::entry & e)
{
	// clang-format off
	return std::visit(overloaded{
		[](std::uint64_t v)       { return fmt::format("{:6}", v); },
		[](std::int64_t v)        { return fmt::format("{:6}", v); },
		[](double v)              { return fmt::format("{:8.2}", v); },
		[](const std::string & v) { return v; },
		[](percent v)             { return to_string(v); },
		[](...)                   { throw std::logic_error {"unhandled type in visit"}; }
		}, e);
	// clang-format on
}
}

void reporter_stream::insert(const std::string & key, const entry & value)
{
	os_ << key << ": " << to_string(value) << '\n';
}

void reporter_stream::set_fields(const std::vector<std::string> & fields)
{
	if (num_fields_ != 0u)
		throw std::runtime_error {"fields or data already set"};
	num_fields_ = fields.size();

	os_ << fmt::format("{}", fmt::join(fields, ";")) << '\n';
}

void reporter_stream::push_back(const std::vector<entry> & data)
{
	if (num_fields_ == 0u)
		num_fields_ = data.size(); // in cases where fields are not defined

	if (num_fields_ != data.size())
		throw std::invalid_argument {"number of data fields differ from head"};

	std::vector<std::string> vs;
	vs.reserve(data.size());
	std::transform(begin(data), end(data), std::back_inserter(vs), format);

	os_ << fmt::format("{}", fmt::join(vs, " | ")) << '\n';
}

void reporter_stream::push_back_group_start()
{
	// intentionally left blank
}

void reporter_stream::push_back_group_end()
{
	os_ << '\n';
}
}

