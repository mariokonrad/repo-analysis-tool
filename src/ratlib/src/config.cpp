#include <ratlib/config.hpp>
#include <nlohmann/json.hpp>
#include <fstream>
#include <regex>

namespace rat
{
using json = nlohmann::json;

namespace
{
static std::string read_format(const json & data)
{
	if (data.find("format") == data.end())
		return {};
	return data["format"].get<std::string>();
}

static std::vector<config::filter> read_filters(const json & data)
{
	if (data.find("filter") == data.end())
		return {};

	static const std::regex key_pattern("^[+-][a-z][a-z0-9_]*$",
		std::regex_constants::ECMAScript | std::regex_constants::icase);

	std::vector<config::filter> result;
	for (const auto & f : data["filter"]) {
		if (f.empty())
			continue;
		if (!f.is_object() || (f.size() > 1u))
			throw std::runtime_error {"invalid object format, must be key:array"};
		const auto i = f.begin();
		if (!std::regex_match(i.key(), key_pattern))
			throw std::runtime_error {"invalid filter ID format: '[+-]identifier'"};
		result.push_back({i.key(), i.value().get<std::vector<std::string>>()});
	}
	return result;
}

static std::vector<config::author> read_authors(const json & data)
{
	if (data.find("alias") == data.end())
		return {};

	std::vector<config::author> result;
	for (const auto & [k, v] : data["alias"].items())
		result.push_back({k, v.get<std::vector<std::string>>()});
	return result;
}

static std::vector<config::group> read_groups(const json & data)
{
	if (data.find("groups") == data.end())
		return {};

	// TODO: custom deserializer?

	std::vector<config::group> result;
	for (const auto & [k, v] : data["groups"].items()) {
		if (v.find("name") == v.end())
			throw std::runtime_error {"missing field in group: 'name'"};
		if (v.find("date") == v.end())
			throw std::runtime_error {"missing field in group: 'date'"};
		if (v.find("members") == v.end())
			throw std::runtime_error {"missing field in group: 'members'"};

		config::group g;
		g.name = v["name"].get<std::string>();
		g.range = date_range::parse(v["date"].get<std::string>());

		if (!v["members"].is_array())
			throw std::runtime_error {"field 'members': expected array"};
		for (const auto & m : v["members"])
			g.members.push_back(m.get<std::string>());
		if (g.members.empty())
			throw std::runtime_error {"field 'members': expected to contain entries"};

		// check if this group already exists and is overlapping
		if (std::find_if(begin(result), end(result),
				[&](const auto & entry) {
					return (g.name == entry.name) && g.range.overlaps(entry.range);
				})
			!= end(result))
			throw std::runtime_error {"self overlapping groups are not allowed"};

		result.push_back(g);
	}

	return result;
}
}

config config::from_file(const std::string & filepath)
{
	std::ifstream ifs {filepath, std::ios::binary | std::ios::ate};
	auto file_size = ifs.tellg();
	std::string content(static_cast<std::string::size_type>(file_size), '\0');
	ifs.seekg(0);
	ifs.read(content.data(), file_size);
	return from_string(content);
}

config config::from_string(const std::string & content)
{
	auto data = json::parse(content);
	auto cfg = config {};

	cfg.format_ = read_format(data);
	cfg.filters_ = read_filters(data);
	cfg.authors_ = read_authors(data);
	cfg.groups_ = read_groups(data);

	return cfg;
}

const std::string & config::format() const
{
	return format_;
}

const std::vector<config::filter> & config::filters() const
{
	return filters_;
}

const std::vector<config::author> & config::authors() const
{
	return authors_;
}

config::author config::author_for(const std::string & alias) const
{
	if (alias.empty())
		return {};

	// first priority: author names
	if (auto i = std::find_if(begin(authors_), end(authors_),
			[&alias](const auto & author) { return author.name == alias; });
		i != end(authors_))
		return *i;

	// second priority: author aliases
	for (const auto & author : authors_) {
		if (std::find_if(begin(author.aliases), end(author.aliases),
				[&alias](const auto & authoralias) { return authoralias == alias; })
			!= end(author.aliases))
			return author;
	}

	return {alias, {}};
}

const std::vector<config::group> & config::groups() const
{
	return groups_;
}
}
