#include "parse_perforce.hpp"
#include "string_split.hpp"
#include "sto.hpp"
#include <date/date.h>
#include <stdexcept>

namespace rat::perforce
{
static date::year_month_day parse_date(const std::string & s)
{
	const auto parts = utils::split<std::vector<std::string>>(s, "/");
	if (parts.size() != 3u)
		throw std::runtime_error{"invalid number of parts in date: " + s};

	return date::year_month_day {date::year(sto<std::uint64_t>(parts[0])),
		date::month(sto<std::uint64_t>(parts[2])), date::day(sto<std::uint64_t>(parts[1]))};
}

static commit_head parse_head(
	const std::string & line, std::function<std::string(const std::string &)> author_resolver)
{
	const auto parts = utils::split<std::vector<std::string>>(line);
	if (parts.size() != 7u)
		throw std::runtime_error{"invalid number of fields in header: " + line};

	const auto origin = utils::split<std::vector<std::string>>(parts[3], "@");
	return {parts[1], parse_date(parts[5]), parts[6], author_resolver(origin[0])};
}

static file_change parse_file_change(const std::string & line)
{
	static const std::string preamble = "... //depot/";
	return {line.substr(preamble.size(), line.find_first_of("#") - preamble.size()), 0u, 0u};
}

std::vector<commit> parse_commits(std::istream & ifs,
	std::function<bool(const commit_head & head, const std::string &)> filter,
	std::function<std::string(const std::string &)> author_resolver)
{
	std::vector<commit> commits;

	commit current;
	for (std::string line; std::getline(ifs, line);) {
		if (line.empty())
			continue;

		const auto line_view = std::string_view(line);
		if (line_view.substr(0, 6) == "Change") {
			// new change, save last
			if (!current.head.id.empty()) {
				if (!current.files.empty())
					commits.push_back(current);
				current = commit{};
			}
			current.head = parse_head(line, author_resolver);
		} else if (line_view.substr(0, 3) == "...") {
			auto change = parse_file_change(line);
			if (filter(current.head, change.filename))
				current.files.push_back(change);
		}
	}
	if (!current.head.id.empty())
		if (!current.files.empty())
			commits.push_back(current);

	return commits;
}
}
