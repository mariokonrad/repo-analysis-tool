#include "parse_git.hpp"
#include "string_split.hpp"
#include "sto.hpp"
#include <date/date.h>
#include <fmt/format.h>

namespace rat::git
{
static date::year_month_day parse_date(const std::string & s)
{
	const auto parts = utils::split<std::vector<std::string>>(s, "-");
	if (parts.size() != 3u)
		throw std::runtime_error{"invalid number of parts in date: " + s};

	return date::year_month_day {date::year(sto<std::uint64_t>(parts[0])),
		date::month(sto<std::uint64_t>(parts[1])), date::day(sto<std::uint64_t>(parts[2]))};
}

static commit_head parse_head(const std::string & line,
	std::function<std::string(const std::string &)> author_resolver)
{
	const auto parts = utils::split<std::vector<std::string>>(line, "#");
	if (parts.size() != 3u)
		throw std::runtime_error{"invalid number of parts in: " + line};
	const auto committer = author_resolver(line.substr(line.find_last_of("#") + 1u));
	return {parts[0], parse_date(parts[1]), {}, committer};
}

static file_change parse_file_change(const std::string & line)
{
	static constexpr const char * whitespace = " \t";

	std::size_t p0 = 0u;
	std::size_t p1 = 0u;

	// additions
	p1 = line.find_first_of(whitespace, p0);
	if (p1 == p0)
		throw std::runtime_error{"invalid format on line: " + line};
	const auto stradd = line.substr(p0, p1 - p0);
	const std::uint64_t additions = (stradd == "-") ? 0u : sto<std::uint64_t>(stradd);
	p0 = p1;

	// whitespaces
	p1 = line.find_first_not_of(whitespace, p0);
	if (p1 == p0)
		throw std::runtime_error{"invalid format on line: " + line};
	p0 = p1;

	// deletions
	p1 = line.find_first_of(whitespace, p0);
	if (p1 == p0)
		throw std::runtime_error{"invalid format on line: " + line};
	const auto strdel = line.substr(p0, p1 - p0);
	const std::uint64_t deletions = (strdel == "-") ? 0u : sto<std::uint64_t>(strdel);
	p0 = p1;

	// whitespaces
	p1 = line.find_first_not_of(whitespace, p0);
	if (p1 == p0)
		throw std::runtime_error{"invalid format on line: " + line};

	return {line.substr(p1), additions, deletions}; // filename may contain spaces
}

std::vector<commit> parse_commits(std::istream & ifs,
	std::function<bool(const commit_head & head, const std::string &)> filter,
	std::function<std::string(const std::string &)> author_resolver)
{
	std::vector<commit> commits;

	commit current;
	for (std::string line; std::getline(ifs, line);) {
		if (line.empty())
			continue;

		const auto line_view = std::string_view(line);
		if (line_view.substr(0, 1) == "#") {
			// new change, save last
			if (!current.head.id.empty()) {
				if (!current.files.empty())
					commits.push_back(current);
				current = commit {};
			}
			current.head = parse_head(line, author_resolver);
		} else {
			auto change = parse_file_change(line);
			if (filter(current.head, change.filename))
				current.files.push_back(change);
		}
	}
	if (!current.head.id.empty())
		if (!current.files.empty())
			commits.push_back(current);

	return commits;
}
}
