#ifndef RAT_PARSE_PERFORCE_HPP
#define RAT_PARSE_PERFORCE_HPP

#include <ratlib/commit.hpp>
#include <functional>
#include <iosfwd>
#include <string>

namespace rat::perforce
{
std::vector<commit> parse_commits(std::istream & ifs,
	std::function<bool(const commit_head &, const std::string &)> filter,
	std::function<std::string(const std::string &)> author_resolver);
}

#endif
