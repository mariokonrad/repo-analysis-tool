#ifndef RAT_FILTER_HPP
#define RAT_FILTER_HPP

#include <regex>
#include <string>

namespace rat
{
struct commit_head; // forward

enum class filter_type { whitelist, blacklist };

class filter
{
public:
	virtual ~filter();
	filter(filter_type type, const std::string & rule);

	filter_type type() const;
	bool check(const commit_head & head, const std::string & filepath) const;
	bool operator()(const commit_head & head, const std::string & filepath) const;

protected:
	virtual bool matches(
		const std::regex &, const commit_head &, const std::string &) const = 0;

private:
	filter_type type_;
	std::regex regex_;
};

class file_filter : public filter
{
public:
	using filter::filter;

protected:
	virtual bool matches(
		const std::regex &, const commit_head &, const std::string &) const override;
};

class author_filter : public filter
{
public:
	using filter::filter;

protected:
	virtual bool matches(
		const std::regex &, const commit_head &, const std::string &) const override;
};

std::unique_ptr<filter> make_filter(const std::string & id, const std::string & rule);
}

#endif
