#ifndef RAT_STATISTICS_HPP
#define RAT_STATISTICS_HPP

#include <iterator>
#include <numeric>
#include <tuple>
#include <cmath>

namespace rat
{
template <class Iterator, class T = typename std::iterator_traits<Iterator>::value_type>
T median(Iterator first, Iterator last)
{
	const auto dist = std::distance(first, last);

	if (dist == 0)
		return {};

	const auto mid = first + (dist - 1) / 2;
	return (dist % 2) ? *mid : (*mid + *(mid + 1)) / 2;
}

template <class T> T sqr(const T & x) noexcept
{
	return x * x;
}

template <class Iterator,
    typename std::enable_if<
        std::is_convertible_v<typename std::iterator_traits<Iterator>::value_type, double>,
        int>::type
    = 0>
std::tuple<double, double> average(Iterator first, Iterator last)
{
	const auto dist = std::distance(first, last);

	if (dist == 0)
		return {0.0, 0.0};

	if (dist < 2)
		return {*first, 0.0};

	const double avg = std::accumulate(first, last, 0.0) / dist;
	const double stddev = std::accumulate(first, last, 0.0, [avg](auto sum, auto a) -> double {
		return sum + sqr(a - avg);
	}) / (dist - 1);

	return {avg, std::sqrt(stddev)};
}
}

#endif

