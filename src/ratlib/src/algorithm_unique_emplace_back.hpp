#ifndef RAT_ALGORITHM_UNIQUE_EMPLACE_BACK_HPP
#define RAT_ALGORITHM_UNIQUE_EMPLACE_BACK_HPP

namespace rat
{
template <class Container, class Predicate, class... Args>
typename Container::value_type & unique_emplace_back(
	Container & c, Predicate pred, Args &&... args)
{
	if (auto i = std::find_if(begin(c), end(c), pred); i != end(c))
		return *i;
	return c.emplace_back(std::forward<Args>(args)...);
}
}

#endif
