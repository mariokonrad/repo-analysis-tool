#include "sto.hpp"
#include <charconv>
#include <stdexcept>

namespace rat
{
template <> std::string sto(const std::string & s)
{
	return s;
}

template <> std::uint64_t sto(const std::string & s)
{
	std::uint64_t result = 0u;
	const auto & [ptr, err] = std::from_chars(s.data(), s.data() + s.size(), result);
	if (ptr != s.data() + s.size())
		throw std::runtime_error {"invalid conversion to uint64 from string: " + s};
	return result;
}
}
