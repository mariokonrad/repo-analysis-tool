#ifndef RAT_CHANGE_COUNT_HPP
#define RAT_CHANGE_COUNT_HPP

#include <string>

namespace rat
{
struct change_count {
	change_count() = default;
	change_count(const std::string & f, std::uint64_t n)
		: name(f)
		, count(n)
	{
	}

	std::string name;
	std::uint64_t count = 0u;
};
}

#endif
