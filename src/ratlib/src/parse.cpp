#include <ratlib/parse.hpp>
#include "parse_git.hpp"
#include "parse_perforce.hpp"
#include <fstream>

namespace rat
{
std::vector<rat::commit> parse(const std::string & format, const std::string & filepath,
	std::function<bool(const rat::commit_head &, const std::string &)> filter,
	std::function<std::string(const std::string &)> author_resolver)
{
	std::ifstream ifs{filepath};

	if (format == "git")
		return rat::git::parse_commits(ifs, filter, author_resolver);
	if (format == "perforce")
		return rat::perforce::parse_commits(ifs, filter, author_resolver);

	throw std::runtime_error {"unsupported format: " + format};
}
}

