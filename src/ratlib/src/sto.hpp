#ifndef RAT_STO_HPP
#define RAT_STO_HPP

#include <string>

namespace rat
{
template <class T> T sto(const std::string &); // primary template
}

#endif
