#ifndef RAT_ANALYSIS_PARAMETERS_HPP
#define RAT_ANALYSIS_PARAMETERS_HPP

#include "sto.hpp"
#include <string>
#include <tuple>
#include <vector>

namespace rat
{
class analysis_parameters
{
public:
	analysis_parameters(const std::vector<std::string> & params);

	template <class T> T get(const std::string & key, const T & default_value) const;

private:
	using key_value = std::tuple<std::string, std::string>;

	std::vector<key_value> params_;

	static key_value split(const std::string & param);
};

template <class T>
T analysis_parameters::get(const std::string & key, const T & default_value) const
{
	for (const auto & [k, v] : params_)
		if (k == key)
			return sto<T>(v);
	return default_value;
}
}

#endif
