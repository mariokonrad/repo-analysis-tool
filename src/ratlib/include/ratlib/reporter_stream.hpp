#ifndef RAT_REPORTER_STREAM_HPP
#define RAT_REPORTER_STREAM_HPP

#include <ratlib/reporter.hpp>
#include <iosfwd>

namespace rat
{
class reporter_stream : public reporter
{
public:
	virtual ~reporter_stream() = default;

	explicit reporter_stream(std::ostream & os)
		: os_(os)
	{
	}

	virtual void insert(const std::string & key, const entry & value) override;

	virtual void set_fields(const std::vector<std::string> & fields) override;

	virtual void push_back(const std::vector<entry> & data) override;
	virtual void push_back_group_start() override;
	virtual void push_back_group_end() override;

private:
	std::ostream & os_;
	std::size_t num_fields_ = 0u;
};
}

#endif
