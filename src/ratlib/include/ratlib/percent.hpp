#ifndef RAT_PERCENT_HPP
#define RAT_PERCENT_HPP

#include <string>
#include <cmath>

namespace rat
{
// Represents a percentage in the range 0..
class percent
{
public:
	explicit percent(std::uint64_t value)
		: value_(value)
	{
	}

	// Takes a percentage as floating point number, useful to be used
	// with results of computations where 0.01 is 1%.
	explicit percent(double value)
		: value_(static_cast<std::uint64_t>(round(value * 100.0)))
	{
	}

	percent(const percent &) = default;
	percent & operator=(const percent &) = default;

	percent(percent &&) = default;
	percent & operator=(percent &&) = default;

	std::uint64_t value() const { return value_; };
	operator std::uint64_t () const { return value(); }

private:
	std::uint64_t value_ = {};
};

std::string to_string(const percent &);
}

#endif
