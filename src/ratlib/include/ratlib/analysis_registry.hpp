#ifndef RAT_ANALYSIS_REGISTRY_HPP
#define RAT_ANALYSIS_REGISTRY_HPP

#include <ratlib/analysis.hpp>
#include <functional>
#include <iosfwd>
#include <memory>
#include <string>
#include <vector>

namespace rat
{
class analysis_registry
{
public:
	static bool check(const std::string & name_and_config);
	static std::vector<std::string> names();
	static std::function<void(std::ostream &)> help(const std::string & name);
	static std::unique_ptr<analysis> create(const std::string & name_and_config);
};
}

#endif
