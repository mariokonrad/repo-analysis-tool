#ifndef RAT_PARSE_HPP
#define RAT_PARSE_HPP

#include <ratlib/commit.hpp>
#include <functional>
#include <string>
#include <vector>

namespace rat
{
std::vector<rat::commit> parse(const std::string & format, const std::string & filepath,
	std::function<bool(const rat::commit_head &, const std::string &)> filter,
	std::function<std::string(const std::string &)> author_resolver);
}

#endif
