#ifndef RAT_REPORTER_HPP
#define RAT_REPORTER_HPP

#include <ratlib/percent.hpp>
#include <string>
#include <variant>
#include <vector>

namespace rat
{
class reporter
{
public:
	using entry = std::variant<std::uint64_t, std::int64_t, double, std::string, percent>;

	virtual ~reporter() = default;

	virtual void insert(const std::string & key, const entry & value) = 0;

	virtual void set_fields(const std::vector<std::string> & fields) = 0;

	virtual void push_back(const std::vector<entry> & data) = 0;
	virtual void push_back_group_start() = 0;
	virtual void push_back_group_end() = 0;
};
}

#endif
