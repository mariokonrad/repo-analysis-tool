#ifndef RAT_COMMIT_HPP
#define RAT_COMMIT_HPP

#include <date/date.h>
#include <string>
#include <vector>

namespace rat {

struct commit_head
{
	std::string id;
	date::year_month_day date = date::sys_days(date::days(0));
	std::string time;
	std::string committer;
};

struct file_change
{
	const std::string filename;
	const std::uint64_t additions = 0u;
	const std::uint64_t deletions = 0u;
};

struct commit
{
	commit_head head;
	std::vector<file_change> files;
};

}

#endif
