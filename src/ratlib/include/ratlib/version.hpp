#ifndef RAT_VERISION_HPP
#define RAT_VERISION_HPP

namespace rat
{
const char * project_name() noexcept;
const char * project_version() noexcept;
const char * project_version_major() noexcept;
const char * project_version_minor() noexcept;
const char * project_version_patch() noexcept;
}

#endif
