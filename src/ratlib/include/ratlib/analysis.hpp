#ifndef RAT_ANALYSIS_HPP
#define RAT_ANALYSIS_HPP

#include <ratlib/commit.hpp>
#include <iosfwd>
#include <string>
#include <vector>

namespace rat
{
class analysis_parameters; // forward declaration
class reporter; // forward declaration

class analysis
{
public:
	virtual ~analysis() = default;

	virtual void process(reporter & rep, const std::vector<commit> & commits) = 0;

	void operator()(reporter & rep, const std::vector<commit> & commits)
	{
		process(rep, commits);
	}
};

struct analysis_descriptor {
	std::function<std::string()> name;
	std::function<std::unique_ptr<analysis>(const analysis_parameters &)> create;
	std::function<void(std::ostream &)> help;
};
}

#endif
