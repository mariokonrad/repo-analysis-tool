#ifndef RAT_CONFIG_HPP
#define RAT_CONFIG_HPP

#include <ratlib/date_range.hpp>
#include <date/date.h>
#include <string>
#include <vector>

namespace rat
{
class config
{
public:
	struct filter {
		std::string id;
		std::vector<std::string> rules;
	};

	struct author {
		std::string name;
		std::vector<std::string> aliases;
	};

	struct group {
		std::string name;
		date_range range;
		std::vector<std::string> members;
	};

	static config from_file(const std::string & filepath);
	static config from_string(const std::string & content);

	config() = default;

	const std::string & format() const;
	const std::vector<filter> & filters() const;
	const std::vector<author> & authors() const;
	author author_for(const std::string & alias) const;
	const std::vector<group> & groups() const;

private:
	std::string format_;
	std::vector<filter> filters_;
	std::vector<author> authors_;
	std::vector<group> groups_;
};
}

#endif
