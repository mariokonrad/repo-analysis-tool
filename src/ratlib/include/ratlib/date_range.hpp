#ifndef RAT_DATE_RANGE_HPP
#define RAT_DATE_RANGE_HPP

#include <date/date.h>

namespace rat
{
class date_range
{
public:
	static date_range parse(const std::string &);

	date_range() = default;
	date_range(const date::year_month_day & f, const date::year_month_day & t);

	const date::year_month_day & from() const noexcept;
	const date::year_month_day & to() const noexcept;

	/// Compares the specified date against the range.
	///
	/// Returns true if the date is contained (inclusive) the range `from..to`.
	bool contains(const date::year_month_day & d) const;

	bool overlaps(const date_range & other) const;

private:
	date::year_month_day from_ = date::year(0) / date::January / 1;
	date::year_month_day to_ = date::year(9999) / date::December / date::last;
};
}

#endif
