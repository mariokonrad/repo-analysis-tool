#ifndef RAT_FILTER_UTILS_HPP
#define RAT_FILTER_UTILS_HPP

#include <functional>
#include <string>

namespace rat
{
struct commit_head; // forwrad
class config; // forward

template <class Iterator>
static bool check_filter_chain(
	Iterator first, Iterator last, const commit_head & head, const std::string & filepath)
{
	bool result = true;
	for (; result && (first != last); ++first)
		result &= (*first)->check(head, filepath);
	return result;
}

auto create_filter(const config & cfg)
	-> std::function<bool(const commit_head &, const std::string &)>;
}

#endif
