#include <ratlib/date_range.hpp>
#include <gtest/gtest.h>

namespace
{
using rat::date_range;

class test_date_range : public ::testing::Test
{
};

TEST_F(test_date_range, empty_string)
{
	EXPECT_ANY_THROW(date_range::parse(""));
}

TEST_F(test_date_range, default_range)
{
	const char * input = "..";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f < t);

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(9999)/date::December/date::last);
}

TEST_F(test_date_range, range_year_until)
{
	const char * input = "2018..";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::January/1);
	EXPECT_EQ(t, date::year(9999)/date::December/date::last);
}

TEST_F(test_date_range, range_year_month_until)
{
	const char * input = "2018-02..";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::February/1);
	EXPECT_EQ(t, date::year(9999)/date::December/date::last);
}

TEST_F(test_date_range, range_year_month_day_until)
{
	const char * input = "2018-03-12..";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::March/12);
	EXPECT_EQ(t, date::year(9999)/date::December/date::last);
}

TEST_F(test_date_range, range_until_year)
{
	const char * input = "..2018";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(2018)/date::December/date::last);
}

TEST_F(test_date_range, range_until_year_month)
{
	const char * input = "..2018-03";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(2018)/date::March/date::last);
}

TEST_F(test_date_range, range_until_year_month_feb_nonleap)
{
	const char * input = "..2018-02";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(2018)/date::February/date::last);
}

TEST_F(test_date_range, range_until_year_month_feb_leap)
{
	const char * input = "..2016-02";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(2016)/date::February/date::last);
}

TEST_F(test_date_range, range_until_year_month_day)
{
	const char * input = "..2018-02-23";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(0)/date::January/1);
	EXPECT_EQ(t, date::year(2018)/date::February/23);
}

TEST_F(test_date_range, range_year_year)
{
	const char * input = "2018..2019";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::January/1);
	EXPECT_EQ(t, date::year(2019)/date::December/date::last);
}

TEST_F(test_date_range, range_year_month_year)
{
	const char * input = "2018-02..2019";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::February/1);
	EXPECT_EQ(t, date::year(2019)/date::December/date::last);
}

TEST_F(test_date_range, range_year_month_day_year)
{
	const char * input = "2018-02-23..2019";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_EQ(f, date::year(2018)/date::February/23);
	EXPECT_EQ(t, date::year(2019)/date::December/date::last);
}

TEST_F(test_date_range, range_contains_date)
{
	struct testcase { const date::year_month_day d; bool expected; };

	static const testcase cases[] = {
		{ date::year(2016)/date::month(12)/31, false },
		{ date::year(2017)/date::month(01)/01, true },
		{ date::year(2017)/date::month(01)/02, true },
		{ date::year(2017)/date::month(02)/01, true },
		{ date::year(2018)/date::month(03)/14, true },
		{ date::year(2018)/date::month(03)/15, true },
		{ date::year(2018)/date::month(03)/16, false },
	};

	const auto r = date_range::parse("2017-01-01..2018-03-15");

	for (const auto & tc : cases)
		EXPECT_TRUE(r.contains(tc.d) == tc.expected);
}

TEST_F(test_date_range, single_date_year_month_day)
{
	const char * input = "2018-03-09";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == t);

	EXPECT_EQ(f, date::year(2018)/date::March/9);
	EXPECT_EQ(t, date::year(2018)/date::March/9);
}

TEST_F(test_date_range, single_date_year_month)
{
	const char * input = "2018-03";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_EQ(f, date::year(2018)/date::March/1);
	EXPECT_EQ(t, date::year(2018)/date::March/date::last);
}

TEST_F(test_date_range, single_date_year_month_feb_leap)
{
	const char * input = "2016-02";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_EQ(f, date::year(2016)/date::February/1);
	EXPECT_EQ(t, date::year(2016)/date::February/date::last);
}

TEST_F(test_date_range, single_date_year_month_feb_nonleap)
{
	const char * input = "2018-02";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_EQ(f, date::year(2018)/date::February/1);
	EXPECT_EQ(t, date::year(2018)/date::February/date::last);
}

TEST_F(test_date_range, single_date_year)
{
	const char * input = "2018";

	const auto r = date_range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_EQ(f, date::year(2018)/date::January/1);
	EXPECT_EQ(t, date::year(2018)/date::December/date::last);
}

TEST_F(test_date_range, single_date_failure)
{
	EXPECT_ANY_THROW(date_range::parse("2018-03-1"));
	EXPECT_ANY_THROW(date_range::parse("2018-12-1"));
	EXPECT_ANY_THROW(date_range::parse("2018-12-32"));
}
}
