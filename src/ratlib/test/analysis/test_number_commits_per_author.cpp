#include "analysis/number_commits_per_author.hpp"
#include "mock_reporter.hpp"

namespace {
class test_number_commits_per_author : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using ::testing::_;
using ::testing::Eq;

TEST_F(test_number_commits_per_author, no_commits)
{
	const std::vector<commit> commits;
	rat::number_commits_per_author a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	a.process(rep, commits);
}

TEST_F(test_number_commits_per_author, multiple_commits_one_author)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::number_commits_per_author a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(4ul), "mk"}))).Times(1);

	a.process(rep, commits);
}

TEST_F(test_number_commits_per_author, multiple_commits_multiple_authors)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "km"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mm"}, {{"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::number_commits_per_author a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(2ul), "mk"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "km"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "mm"}))).Times(1);

	a.process(rep, commits);
}
}

