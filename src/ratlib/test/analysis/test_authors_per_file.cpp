#include "analysis/authors_per_file.hpp"
#include "mock_reporter.hpp"

namespace {
class test_authors_per_file : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using ::testing::_;
using ::testing::Eq;

TEST_F(test_authors_per_file, no_commits)
{
	const std::vector<commit> commits;
	rat::authors_per_changed_file a;
	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	a.process(rep, commits);
}

TEST_F(test_authors_per_file, multiple_commits_unique_file_per_commit_sinlge_author_threshold_0)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::authors_per_changed_file a(0u);
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(4);

	a.process(rep, commits);
}

TEST_F(test_authors_per_file, multiple_commits_unique_file_per_commit_sinlge_author_threshold_1)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::authors_per_changed_file a(1u);
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(4);

	a.process(rep, commits);
}

TEST_F(test_authors_per_file, multiple_commits_unique_file_per_commit_sinlge_author_threshold_2)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::authors_per_changed_file a(2u);
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	a.process(rep, commits);
}

TEST_F(test_authors_per_file, multiple_commits_unique_file_per_commit_multiple_author_threshold_1)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "km"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mm"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::authors_per_changed_file a(1u);
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "mk", "file1.cpp"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "mk", "file2.cpp"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "km", "file3.cpp"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "mm", "file4.cpp"}))).Times(1);

	a.process(rep, commits);
}

TEST_F(test_authors_per_file, multiple_commits_multiple_author_threshold_1)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "km"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mm"}, {{"file3.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::authors_per_changed_file a(1u);
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(2ul), "km;mk", "file1.cpp"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "km", "file2.cpp"}))).Times(1);
	EXPECT_CALL(rep, push_back(Eq(mock_reporter::entries{entry(1ul), "mm", "file3.cpp"}))).Times(1);

	a.process(rep, commits);
}
}

