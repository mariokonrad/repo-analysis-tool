#include "analysis/file_changes.hpp"
#include "mock_reporter.hpp"

namespace {
class test_file_changes : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using ::testing::_;
using ::testing::Eq;

TEST_F(test_file_changes, no_commits)
{
	const std::vector<commit> commits;
	rat::file_changes a;
	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(2);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	a.process(rep, commits);
}

TEST_F(test_file_changes, multiple_commits_unique_file_per_commit_threshold_0)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_changes a(0u);
	mock_reporter rep;

	EXPECT_CALL(rep, insert(Eq("threshold"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("number of changed files over threshold"), Eq(entry(4lu)))).Times(1);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(4);

	a.process(rep, commits);
}

TEST_F(test_file_changes, multiple_commits_unique_file_per_commit_threshold_1)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_changes a(1u);
	mock_reporter rep;

	EXPECT_CALL(rep, insert(Eq("threshold"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("number of changed files over threshold"), Eq(entry(4lu)))).Times(1);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(4);

	a.process(rep, commits);
}

TEST_F(test_file_changes, multiple_commits_one_file_multiple_changes_threshold_2)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}, {"file1.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file3.cpp", 0u, 0u}, {"file1.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file4.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_changes a(2u);
	mock_reporter rep;

	EXPECT_CALL(rep, insert(Eq("threshold"), Eq(entry(2lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("number of changed files over threshold"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(1);

	a.process(rep, commits);
}
}

