#include "analysis/overview.hpp"
#include "mock_reporter.hpp"

namespace {
class test_overview : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using ::testing::_;
using ::testing::Eq;
using ::testing::DoubleNear;
using ::testing::VariantWith;

TEST_F(test_overview, no_commits)
{
	const std::vector<commit> commits;
	rat::overview a;
	mock_reporter rep;

	EXPECT_CALL(rep, insert(Eq("commits"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("authors"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files unique"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files total"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("median number changed files per commit"), Eq(entry(0lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("average number change files per commit"), Eq(entry(0.0)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("stddev number change files per commit"), Eq(entry(0.0)))).Times(1);

	a.process(rep, commits);
}

TEST_F(test_overview, single_commits)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"abcdef", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::overview a;
	mock_reporter rep;

	EXPECT_CALL(rep, insert(Eq("commits"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("authors"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files unique"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files total"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("median number changed files per commit"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("average number change files per commit"), Eq(entry(1.0)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("stddev number change files per commit"), Eq(entry(0.0)))).Times(1);

	a.process(rep, commits);
}

TEST_F(test_overview, multiple_commits)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::overview a;
	mock_reporter rep;

	// clang-format off
	EXPECT_CALL(rep, insert(Eq("commits"), Eq(entry(4lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("authors"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files unique"), Eq(entry(2lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("changed files total"), Eq(entry(6lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("median number changed files per commit"), Eq(entry(1lu)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("average number change files per commit"), VariantWith<double>(DoubleNear(1.5, 1e-6)))).Times(1);
	EXPECT_CALL(rep, insert(Eq("stddev number change files per commit"), VariantWith<double>(DoubleNear(0.577350269, 1e-6)))).Times(1);
	// clang-format on

	a.process(rep, commits);
}
}

