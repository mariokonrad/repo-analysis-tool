#include "analysis/file_age.hpp"
#include "mock_reporter.hpp"

namespace
{
class test_file_age : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using ::testing::_;
using ::testing::Eq;

TEST_F(test_file_age, no_commits)
{
	const std::vector<commit> commits;
	rat::file_age a;
	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	a.process(rep, commits);
}

TEST_F(test_file_age, single_commit_single_file)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", date::year{2020}/date::January/1, {}, "mk"}, {{"file1.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_age a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {
			entry(0ul), entry(0ul), entry(1ul), "2020-01-01", "2020-01-01", "file1.cpp"})))
		.Times(1);

	a.process(rep, commits);
}

TEST_F(test_file_age, multiple_commits_single_file)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", date::year{2020}/date::January/1, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", date::year{2020}/date::January/2, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa3", date::year{2020}/date::January/3, {}, "mk"}, {{"file1.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_age a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {
			entry(2ul), entry(0ul), entry(3ul), "2020-01-01", "2020-01-03", "file1.cpp"})))
		.Times(1);

	a.process(rep, commits);
}

TEST_F(test_file_age, multiple_commits_multiple_files)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", date::year{2020}/date::January/1, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", date::year{2020}/date::January/2, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", date::year{2020}/date::January/3, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", date::year{2020}/date::January/4, {}, "mk"}, {{"file3.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_age a;
	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {
			entry(2ul), entry(1ul), entry(3ul), "2020-01-01", "2020-01-03", "file1.cpp"})))
		.Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {
			entry(1ul), entry(1ul), entry(2ul), "2020-01-02", "2020-01-03", "file2.cpp"})))
		.Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {
			entry(0ul), entry(0ul), entry(1ul), "2020-01-04", "2020-01-04", "file3.cpp"})))
		.Times(1);

	a.process(rep, commits);
}
}

