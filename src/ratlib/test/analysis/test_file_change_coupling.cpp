#include "analysis/file_change_coupling.hpp"
#include "mock_reporter.hpp"
#include <gtest/gtest.h>

namespace
{
class test_file_change_coupling : public ::testing::Test
{
};

using rat::commit;
using rat::test::mock_reporter;
using entry = rat::reporter::entry;
using rat::percent;
using ::testing::_;
using ::testing::Eq;

TEST_F(test_file_change_coupling, no_commits)
{
	const std::vector<commit> commits;
	rat::file_change_coupling fcc(1u, 3u, 50u);

	mock_reporter rep;

	EXPECT_CALL(rep, set_fields(_)).Times(0);

	fcc.process(rep, commits);
}

TEST_F(test_file_change_coupling, one_commit_one_file)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"abcdef", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_change_coupling fcc(1u, 1u, 50u);

	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	fcc.process(rep, commits);
}

TEST_F(test_file_change_coupling, two_commits_two_files_independent)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_change_coupling fcc(1u, 2u, 50u);

	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep, push_back(_)).Times(0);

	fcc.process(rep, commits);
}

TEST_F(test_file_change_coupling, two_commits_two_files_dependent)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_change_coupling fcc(1u, 2u, 50u);

	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {entry(percent(100ul)), entry(2ul), "file1.cpp"})))
		.Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {entry(percent(100ul)), entry(2ul), "file2.cpp"})))
		.Times(1);
	EXPECT_CALL(rep, push_back_group_start()).Times(1);
	EXPECT_CALL(rep, push_back_group_end()).Times(1);

	fcc.process(rep, commits);
}

TEST_F(test_file_change_coupling, four_commits_two_files_dependent)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}}
	};
	// clang-format on
	rat::file_change_coupling fcc(1u, 4u, 50u);

	mock_reporter rep;

	EXPECT_CALL(rep, insert(_, _)).Times(0);
	EXPECT_CALL(rep, set_fields(_)).Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {entry(percent(66ul)), entry(3ul), "file1.cpp"})))
		.Times(1);
	EXPECT_CALL(rep,
		push_back(Eq(mock_reporter::entries {entry(percent(66ul)), entry(3ul), "file2.cpp"})))
		.Times(1);
	EXPECT_CALL(rep, push_back_group_start()).Times(1);
	EXPECT_CALL(rep, push_back_group_end()).Times(1);

	fcc.process(rep, commits);
}

TEST_F(test_file_change_coupling, span_matrix_four_commits_two_files_dependent)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}}
	};

	const auto expected = rat::detail::change_matrix{{
		{{3u, 2u}},
		{{2u, 3u}}
	}};
	// clang-format off

	const auto files = rat::detail::file_list_in_span(begin(commits), end(commits), 1u);
	const auto m = rat::detail::span_change_matrix(begin(commits), end(commits), files);

	EXPECT_TRUE(m == expected);
}

TEST_F(test_file_change_coupling, span_matrix_six_commits_three_files_dependent)
{
	// clang-format off
	const std::vector<commit> commits = {
		{{"aa1", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file3.cpp", 0u, 0u}}},
		{{"aa2", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}, {"file3.cpp", 0u, 0u}}},
		{{"aa3", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file2.cpp", 0u, 0u}, {"file3.cpp", 0u, 0u}}},
		{{"aa4", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}}},
		{{"aa5", {}, {}, "mk"}, {{"file2.cpp", 0u, 0u}}},
		{{"aa6", {}, {}, "mk"}, {{"file1.cpp", 0u, 0u}, {"file3.cpp", 0u, 0u}}},
	};

	const auto expected = rat::detail::change_matrix{{
		{{4u, 1u, 3u}},
		{{1u, 3u, 2u}},
		{{3u, 2u, 4u}}
	}};
	// clang-format off

	const auto files = rat::detail::file_list_in_span(begin(commits), end(commits), 1u);
	const auto m = rat::detail::span_change_matrix(begin(commits), end(commits), files);

	EXPECT_TRUE(m == expected);
}

TEST_F(test_file_change_coupling, reduce_span_empty)
{
	using namespace rat::detail;

	const std::vector<couplings_commit_span> span = {};

	const auto result = reduce(span);

	EXPECT_TRUE(result.empty());
}

TEST_F(test_file_change_coupling, reduce_span_single_span)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 100u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 100u);
	EXPECT_TRUE(result[0].files.size() == 2u);
}

TEST_F(test_file_change_coupling, reduce_span_multiple_span_multiple_detection)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 100u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 100u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 100u);
	EXPECT_TRUE(result[0].files.size() == 2u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_increasing_percent)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 90u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 90u);
	EXPECT_TRUE(result[0].files.size() == 2u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_decreasing_percent)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 90u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 90u);
	EXPECT_TRUE(result[0].files.size() == 2u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_overlapping_sets_second_in_first)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}, {"file3", 1}}, 90u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 90u);
	EXPECT_TRUE(result[0].files.size() == 3u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_overlapping_sets_first_in_second)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}, {"file3", 1}}, 90u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 1u);
	EXPECT_TRUE(result[0].percent == 90u);
	EXPECT_TRUE(result[0].files.size() == 3u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_overlapping_but_disjunct_sets)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file3", 1}, {"file4", 1}}, 90u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 2u);
	EXPECT_TRUE(result[0].percent == 80u);
	EXPECT_TRUE(result[0].files.size() == 2u);
	EXPECT_TRUE(result[1].percent == 90u);
	EXPECT_TRUE(result[1].files.size() == 3u);
}

TEST_F(test_file_change_coupling,
	reduce_span_multiple_span_multiple_detection_independent_sets)
{
	using namespace rat::detail;

	// clang-format off
	const std::vector<couplings_commit_span> span = {
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file1", 1}, {"file2", 1}}, 80u}
			}
		},
		// span
		{
			// commits
			{}, {},
			// couplings
			{
				{{{"file3", 1}, {"file4", 1}}, 90u}
			}
		},
	};
	// clang-format on

	const auto result = reduce(span);

	EXPECT_TRUE(result.size() == 2u);
	EXPECT_TRUE(result[0].percent == 80u);
	EXPECT_TRUE(result[0].files.size() == 2u);
	EXPECT_TRUE(result[1].percent == 90u);
	EXPECT_TRUE(result[1].files.size() == 2u);
}
}
