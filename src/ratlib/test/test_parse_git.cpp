#include "parse_git.hpp"
#include <gtest/gtest.h>
#include <date/date.h>
#include <sstream>

namespace
{
static bool null_filter(const rat::commit_head &, const std::string &)
{
	return true;
}

static std::string null_author_mapper(const std::string & s)
{
	return s;
}

class test_parse_git : public ::testing::Test
{
};

TEST_F(test_parse_git, empty_string)
{
	std::istringstream is;

	const auto result = rat::git::parse_commits(is, null_filter, null_author_mapper);

	EXPECT_TRUE(result.empty());
}

TEST_F(test_parse_git, valid_log)
{
	const std::string log = "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "11	1	src/analysis/authors_per_file.cpp\n"
	                        "11	1	src/analysis/file_changes.cpp\n"
	                        "2	1	src/analysis/number_commits_per_author.cpp\n"
	                        "5	4	src/analysis/overview.cpp\n"
	                        "13	5	src/analysis_registry.cpp\n"
	                        "14	4	src/analysis_registry.hpp\n"
	                        "9	3	src/cli_arguments.cpp\n"
	                        "2	0	src/cli_arguments.hpp\n"
	                        "14	9	src/main.cpp\n";
	std::istringstream is {log};

	const auto result = rat::git::parse_commits(is, null_filter, null_author_mapper);

	EXPECT_TRUE(result.size() == 1u);

	const auto & c = result[0u];
	EXPECT_TRUE(c.head.id == "530a8d3");
	EXPECT_TRUE(
	    c.head.date == date::year_month_day(date::year {2019}, date::August, date::day {5}));
	EXPECT_TRUE(c.head.time.empty());
	EXPECT_TRUE(c.head.committer == "Mario Konrad");
	EXPECT_TRUE(c.files.size() == 9u);
}

TEST_F(test_parse_git, missing_commit_hash)
{
	const std::string log = "##2019-08-05#Mario Konrad\n"
	                        "11	1	src/analysis/authors_per_file.cpp\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, missing_date)
{
	const std::string log = "#530a8d3##Mario Konrad\n"
	                        "11	1	src/analysis/authors_per_file.cpp\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, missing_committer)
{
	const std::string log = "#530a8d3#2019-08-05#\n"
	                        "11	1	src/analysis/authors_per_file.cpp\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, missing_additions)
{
	const std::string log = "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "	1	src/analysis/authors_per_file.cpp\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, missing_deletions)
{
	const std::string log = "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "11		src/analysis/authors_per_file.cpp\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, missing_filename)
{
	const std::string log = "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "11	1	\n";
	std::istringstream is {log};

	EXPECT_ANY_THROW(rat::git::parse_commits(is, null_filter, null_author_mapper));
}

TEST_F(test_parse_git, binary_files_no_add_del_information)
{
	const std::string log = "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "-	-	src/analysis/authors_per_file.png\n";
	std::istringstream is {log};

	const auto result = rat::git::parse_commits(is, null_filter, null_author_mapper);

	EXPECT_TRUE(result.size() == 1u);

	const auto & c = result[0u];
	EXPECT_TRUE(c.head.id == "530a8d3");
	EXPECT_TRUE(
	    c.head.date == date::year_month_day(date::year {2019}, date::August, date::day {5}));
	EXPECT_TRUE(c.head.time.empty());
	EXPECT_TRUE(c.head.committer == "Mario Konrad");
	EXPECT_TRUE(c.files.size() == 1u);
}

TEST_F(test_parse_git, merge_commits)
{
	const std::string log = "#abcdef0#2019-08-06#Mario Konrad\n"
	                        "#530a8d3#2019-08-05#Mario Konrad\n"
	                        "11	1	src/analysis/authors_per_file.cpp\n"
	                        "11	1	src/analysis/file_changes.cpp\n"
	                        "2	1	src/analysis/number_commits_per_author.cpp\n"
	                        "5	4	src/analysis/overview.cpp\n"
	                        "13	5	src/analysis_registry.cpp\n"
	                        "14	4	src/analysis_registry.hpp\n"
	                        "9	3	src/cli_arguments.cpp\n"
	                        "2	0	src/cli_arguments.hpp\n"
	                        "14	9	src/main.cpp\n";
	std::istringstream is {log};

	const auto result = rat::git::parse_commits(is, null_filter, null_author_mapper);

	EXPECT_TRUE(result.size() == 1u);

	const auto & c = result[0u];
	EXPECT_TRUE(c.head.id == "530a8d3");
	EXPECT_TRUE(
	    c.head.date == date::year_month_day(date::year {2019}, date::August, date::day {5}));
	EXPECT_TRUE(c.head.time.empty());
	EXPECT_TRUE(c.head.committer == "Mario Konrad");
	EXPECT_TRUE(c.files.size() == 9u);
}
}
