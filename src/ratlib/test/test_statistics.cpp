#include "statistics.hpp"
#include <gtest/gtest.h>
#include <vector>

namespace
{
class test_statistics : public ::testing::Test
{
};

TEST_F(test_statistics, median)
{
	{
		const auto data = std::vector{10};
		EXPECT_NEAR(10, rat::median(begin(data), end(data)), 1e-6);
	}
	{
		const auto data = std::vector{10, 20};
		EXPECT_NEAR(15, rat::median(begin(data), end(data)), 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30};
		EXPECT_NEAR(20, rat::median(begin(data), end(data)), 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30, 40};
		EXPECT_NEAR(25, rat::median(begin(data), end(data)), 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30, 40, 50};
		EXPECT_NEAR(30, rat::median(begin(data), end(data)), 1e-6);
	}
}

TEST_F(test_statistics, average)
{
	{
		const auto data = std::vector {10};
		const auto [a, s] = rat::average(begin(data), end(data));
		EXPECT_NEAR(10, a, 1e-6);
		EXPECT_NEAR(0, s, 1e-6);
	}
	{
		const auto data = std::vector {10, 20};
		const auto [a, s] = rat::average(begin(data), end(data));
		EXPECT_NEAR(15, a, 1e-6);
		EXPECT_NEAR(7.07106781, s, 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30};
		const auto [a,s] = rat::average(begin(data), end(data));
		EXPECT_NEAR(20, a, 1e-6);
		EXPECT_NEAR(10, s, 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30, 40};
		const auto [a,s] = rat::average(begin(data), end(data));
		EXPECT_NEAR(25, a, 1e-6);
		EXPECT_NEAR(12.90994449, s, 1e-6);
	}
	{
		const auto data = std::vector{10, 20, 30, 40, 50};
		const auto [a,s] = rat::average(begin(data), end(data));
		EXPECT_NEAR(30, a, 1e-6);
		EXPECT_NEAR(15.8113883, s, 1e-6);
	}
}
}
