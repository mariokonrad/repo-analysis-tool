#include <ratlib/config.hpp>
#include <gtest/gtest.h>

namespace
{
class test_config : public ::testing::Test
{
};

using config = rat::config;

TEST_F(test_config, empty_string)
{
	EXPECT_ANY_THROW(rat::config::from_string(""));
}

TEST_F(test_config, empty_object)
{
	EXPECT_NO_THROW(rat::config::from_string("{}"));
}

TEST_F(test_config, default_values)
{
	const auto cfg = config::from_string("{}");

	EXPECT_EQ(0u, cfg.format().size());
	EXPECT_EQ(0u, cfg.filters().size());
	EXPECT_EQ(0u, cfg.authors().size());
}

TEST_F(test_config, field_format_git)
{
	const auto cfg = config::from_string("{\"format\":\"git\"}");
	EXPECT_TRUE(cfg.format() == "git");
}

TEST_F(test_config, field_format_perforce)
{
	const auto cfg = config::from_string("{\"format\":\"perforce\"}");
	EXPECT_TRUE(cfg.format() == "perforce");
}

TEST_F(test_config, field_filter_empty)
{
	static const char * text = "{"
							   "\"filter\": []"
							   "}";
	const auto cfg = config::from_string(text);

	const auto & f = cfg.filters();
	EXPECT_EQ(0u, f.size());
}

TEST_F(test_config, field_filter_one_entry)
{
	static const char * text = "{"
							   "\"filter\": ["
							   "    { \"+file\": [\"^.*$\"] }"
							   "  ]"
							   "}";
	const auto cfg = config::from_string(text);

	const auto & f = cfg.filters();
	EXPECT_EQ(1u, f.size());
}

TEST_F(test_config, field_filter_multiple_rules)
{
	static const char * text = "{"
							   "\"filter\": ["
							   "    { \"+file\":   [\"^.*$\"] },"
							   "    { \"-file\":   [\"^.*$\"] },"
							   "    { \"-author\": [\"^.*$\"] }"
							   "  ]"
							   "}";
	const auto cfg = config::from_string(text);

	const auto & f = cfg.filters();
	EXPECT_EQ(3u, f.size());
}

TEST_F(test_config, field_alias_emtpy)
{
	static const char * text = "{"
							   "  \"alias\":{"
							   "  }"
							   "}";
	const auto cfg = config::from_string(text);

	const auto author = cfg.author_for("foo");
	EXPECT_TRUE(author.name == "foo");
}

TEST_F(test_config, field_alias_name_only)
{
	static const char * text = "{"
							   "  \"alias\":{"
							   "    \"foo\":[]"
							   "  }"
							   "}";
	const auto cfg = config::from_string(text);

	const auto author = cfg.author_for("foo");
	EXPECT_TRUE(author.name == "foo");
	EXPECT_EQ(0u, author.aliases.size());
}

TEST_F(test_config, field_alias_with_aliases)
{
	static const char * text = "{"
							   "  \"alias\":{"
							   "    \"foo\":[\"foo1\", \"foo2\"]"
							   "  }"
							   "}";
	const auto cfg = config::from_string(text);

	const auto author = cfg.author_for("foo");
	EXPECT_TRUE(author.name == "foo");
	EXPECT_EQ(2u, author.aliases.size());
}

TEST_F(test_config, field_alias_found_by_aliases)
{
	static const char * text = "{"
							   "  \"alias\":{"
							   "    \"foo\":[\"foo1\", \"foo2\"]"
							   "  }"
							   "}";
	const auto cfg = config::from_string(text);

	const auto author = cfg.author_for("foo1");
	EXPECT_TRUE(author.name == "foo");
	EXPECT_EQ(2u, author.aliases.size());
}

TEST_F(test_config, groups_non_existent)
{
	static const char * text = "{}";
	const auto cfg = config::from_string(text);

	EXPECT_TRUE(cfg.groups().empty());
}

TEST_F(test_config, groups_is_empty)
{
	static const char * text = "{"
							   "  \"groups\":[]"
							   "}";
	const auto cfg = config::from_string(text);

	EXPECT_TRUE(cfg.groups().empty());
}

TEST_F(test_config, group_is_missing_name)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_is_missing_date)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"members\": []"
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_is_missing_members)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\""
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_is_members_are_empty)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": []"
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_is_members_is_not_an_array)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": \"string\""
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_invalid_date)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"20-01-01..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}

TEST_F(test_config, group_valid_dates)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    }"
							   "  ]"
							   "}";

	const auto cfg = config::from_string(text);

	ASSERT_EQ(1u, cfg.groups().size());
	const auto g = cfg.groups()[0];

	EXPECT_EQ(date::year(2020) / date::January / 1, g.range.from());
	EXPECT_EQ(date::year(2020) / date::February / 1, g.range.to());
}

TEST_F(test_config, group_valid_dates_open_beginning)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    }"
							   "  ]"
							   "}";

	const auto cfg = config::from_string(text);

	ASSERT_EQ(1u, cfg.groups().size());
	const auto g = cfg.groups()[0];

	EXPECT_EQ(date::year(0) / date::January / 1, g.range.from());
	EXPECT_EQ(date::year(2020) / date::February / 1, g.range.to());
}

TEST_F(test_config, group_valid_dates_open_ending)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..\","
							   "        \"members\": [\"foo\"]"
							   "    }"
							   "  ]"
							   "}";

	const auto cfg = config::from_string(text);

	ASSERT_EQ(1u, cfg.groups().size());
	const auto g = cfg.groups()[0];

	EXPECT_EQ(date::year(2020) / date::January / 1, g.range.from());
	EXPECT_EQ(date::year(9999) / date::December / date::last, g.range.to());
}

TEST_F(test_config, group_multiple_groups)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    },"
							   "    {"
							   "        \"name\": \"B\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"bar\"]"
							   "    }"
							   "  ]"
							   "}";

	const auto cfg = config::from_string(text);

	EXPECT_EQ(2u, cfg.groups().size());
}

TEST_F(test_config, group_multiple_groups_same_name_overlapping_dates)
{
	static const char * text = "{"
							   "  \"groups\":["
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"foo\"]"
							   "    },"
							   "    {"
							   "        \"name\": \"A\","
							   "        \"date\": \"2020-01-01..2020-02-01\","
							   "        \"members\": [\"bar\"]"
							   "    }"
							   "  ]"
							   "}";

	EXPECT_ANY_THROW(config::from_string(text));
}
}
