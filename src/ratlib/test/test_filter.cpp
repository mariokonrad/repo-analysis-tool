#include "filter.hpp"
#include <ratlib/commit.hpp>
#include <gtest/gtest.h>

namespace
{
class test_filter : public ::testing::Test
{
protected:
	static constexpr const date::year_month_day test_date = {date::year{2019}, date::January, date::day{0}};
};

TEST_F(test_filter, file_whitelist)
{
	const rat::file_filter f(rat::filter_type::whitelist, "^.*\\.cpp");
	const rat::commit_head head;

	EXPECT_TRUE(f(head, "foobar.cpp"));
	EXPECT_FALSE(f(head, "foobar.hpp"));
}

TEST_F(test_filter, file_blacklist)
{
	const rat::file_filter f(rat::filter_type::blacklist, "^.*\\.cpp");
	const rat::commit_head head;

	EXPECT_FALSE(f(head, "foobar.cpp"));
	EXPECT_TRUE(f(head, "foobar.hpp"));
}

TEST_F(test_filter, author_whitelist)
{
	const rat::author_filter f(rat::filter_type::whitelist, "^bjarne$");

	EXPECT_TRUE(f({"id", test_date, "12:34", "bjarne"}, "foobar.cpp"));
	EXPECT_FALSE(f({"id", test_date, "12:34", "herb"}, "foobar.cpp"));
}

TEST_F(test_filter, author_blacklist)
{
	const rat::author_filter f(rat::filter_type::blacklist, "^bjarne$");

	EXPECT_FALSE(f({"id", test_date, "12:34", "bjarne"}, "foobar.cpp"));
	EXPECT_TRUE(f({"id", test_date, "12:34", "herb"}, "foobar.cpp"));
}

TEST_F(test_filter, make_filter_whitelist_successful)
{
	const auto f = rat::make_filter("+file", "^.*$");
	EXPECT_EQ(rat::filter_type::whitelist, f->type());
}

TEST_F(test_filter, make_filter_no_type_failure)
{
	EXPECT_ANY_THROW(rat::make_filter("file", "^.*$"));
}

TEST_F(test_filter, make_filter_no_name_whitelist_failure)
{
	EXPECT_ANY_THROW(rat::make_filter("+", "^.*$"));
}

TEST_F(test_filter, make_filter_invalid_name_whitelist_failure)
{
	EXPECT_ANY_THROW(rat::make_filter("+foobar", "^.*$"));
}

TEST_F(test_filter, make_filter_file_filter_invalid_type_failure)
{
	EXPECT_ANY_THROW(rat::make_filter("?file", "^.*$"));
}

TEST_F(test_filter, make_filter_file_filter_blacklist_empty_regex)
{
	EXPECT_NO_THROW(rat::make_filter("-file", ""));
}
}
