#ifndef RAT_TEST_MOCK_REPORTER_HPP
#define RAT_TEST_MOCK_REPORTER_HPP

#include <ratlib/reporter.hpp>
#include <gmock/gmock.h>

namespace rat::test
{
class mock_reporter : virtual public rat::reporter
{
public:
	using strings = std::vector<std::string>;
	using entries = std::vector<rat::reporter::entry>;

	MOCK_METHOD(void, insert, (const std::string &, const reporter::entry &), (override));
	MOCK_METHOD(void, set_fields, (const strings &), (override));
	MOCK_METHOD(void, push_back, (const entries &), (override));
	MOCK_METHOD(void, push_back_group_start, (), (override));
	MOCK_METHOD(void, push_back_group_end, (), (override));
};
}

#endif

