#!/bin/bash -e

num_commits=$1

#git log --all --numstat --date=short --pretty=format:'#%h#%ad#%aN' --no-renames --after=YYYY-MM-DD
git log --all --numstat --date=short --pretty=format:'#%h#%ad#%aN' --no-renames -n ${num_commits}

