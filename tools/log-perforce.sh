#!/bin/bash -e

num_commits=$1
depot_path=${2:-//depot/...}

p4 changes -s submitted -m ${num_commits} ${depot_path} \
	| cut -d ' ' -f 2 \
	| xargs -I commitid -n1 sh -c 'p4 describe -s commitid | grep -v "^\s*$" && echo ""'

