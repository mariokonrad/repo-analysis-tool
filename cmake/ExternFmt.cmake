include(ExternalProject)
include(GNUInstallDirs)

if(EXISTS $ENV{HOME}/local/repo/fmt)
	set(repo "file://$ENV{HOME}/local/repo/fmt")
else()
	set(repo "https://github.com/fmtlib/fmt")
endif()
message(STATUS "Extern: fmt ${repo}")

set(extern_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/local")
file(MAKE_DIRECTORY "${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}")

ExternalProject_add(extern-fmt
	GIT_REPOSITORY ${repo}
	GIT_TAG 6.2.0
	GIT_SHALLOW TRUE

	CMAKE_ARGS
		-DCMAKE_BUILD_TYPE=Release
		-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
		-DCMAKE_INSTALL_PREFIX=${extern_INSTALL_PREFIX}
		-DBUILD_SHARED_LIBS=FALSE
		-DFMT_DOC=NO
		-DFMT_INSTALL=YES
		-DFMT_TEST=NO

	INSTALL_DIR ${extern_INSTALL_PREFIX}
	)

add_library(fmt::fmt STATIC IMPORTED)
set_target_properties(fmt::fmt
	PROPERTIES
		IMPORTED_LOCATION
			${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}fmt${CMAKE_STATIC_LIBRARY_SUFFIX}
		INTERFACE_INCLUDE_DIRECTORIES
			${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}
	)
add_dependencies(fmt::fmt extern-fmt)

