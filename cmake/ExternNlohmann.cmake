include(ExternalProject)
include(GNUInstallDirs)

if(EXISTS $ENV{HOME}/local/repo/json)
	set(repo "file://$ENV{HOME}/local/repo/json")
else()
	set(repo "https://github.com/nlohmann/json")
endif()
message(STATUS "Extern: nlohmann/json ${repo}")

set(extern_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/local")
file(MAKE_DIRECTORY "${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}")

ExternalProject_add(extern-nlohmann
	GIT_REPOSITORY ${repo}
	GIT_TAG v3.8.0
	GIT_SHALLOW TRUE

	CMAKE_ARGS
		-DCMAKE_BUILD_TYPE=Release
		-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
		-DCMAKE_INSTALL_PREFIX=${extern_INSTALL_PREFIX}
		-DJSON_BuildTests=FALSE
		-DJSON_Install=TRUE
		-DJSON_MultipleHeaders=FALSE

	INSTALL_DIR ${extern_INSTALL_PREFIX}
	)

add_library(nlohmann::nlohmann INTERFACE IMPORTED)
set_target_properties(nlohmann::nlohmann
	PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES
			${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}
	)
add_dependencies(nlohmann::nlohmann extern-nlohmann)

