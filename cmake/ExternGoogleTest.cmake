include(FetchContent)

if(EXISTS $ENV{HOME}/local/repo/googletest)
	set(repo "file://$ENV{HOME}/local/repo/googletest")
else()
	set(repo "https://github.com/google/googletest.git")
endif()
message(STATUS "Extern: googletest ${repo}")

FetchContent_Declare(
	googletest
	GIT_REPOSITORY ${repo}
	GIT_TAG release-1.11.0
	GIT_SHALLOW TRUE
)

set(BUILD_GMOCK TRUE CACHE BOOL "" FORCE) # do not repeat option, because of description
FetchContent_MakeAvailable(googletest)

