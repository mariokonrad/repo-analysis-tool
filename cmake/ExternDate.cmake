include(ExternalProject)
include(GNUInstallDirs)

if(EXISTS $ENV{HOME}/local/repo/date)
	set(repo "file://$ENV{HOME}/local/repo/date")
else()
	set(repo "https://github.com/HowardHinnant/date.git")
endif()
message(STATUS "Extern: date ${repo}")

set(extern_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/local")
file(MAKE_DIRECTORY "${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}")

ExternalProject_add(extern-date
	GIT_REPOSITORY ${repo}
	GIT_TAG v2.4.1
	GIT_SHALLOW TRUE

	CMAKE_ARGS
		-DCMAKE_BUILD_TYPE=Release
		-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
		-DCMAKE_INSTALL_PREFIX=${extern_INSTALL_PREFIX}
		-DBUILD_SHARED_LIBS=FALSE
		-DUSE_SYSTEM_TZ_DB=TRUE
		-DUSE_TZ_DB_IN_DOT=FALSE
		-DENABLE_DATE_TESTING=FALSE
	INSTALL_DIR ${extern_INSTALL_PREFIX}
	)

add_library(date::date STATIC IMPORTED)
set_target_properties(date::date
	PROPERTIES
		IMPORTED_LOCATION
			${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}tz${CMAKE_STATIC_LIBRARY_SUFFIX}
		INTERFACE_INCLUDE_DIRECTORIES
			${extern_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}
	)
add_dependencies(date::date extern-date)

